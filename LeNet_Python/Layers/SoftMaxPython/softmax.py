import numpy as np
import time

arr = [0.0205,0.0043,0.0105,0.0323,0.0345,0.0009,0.00134,0.989,0.045,0.0543]

def softmax(sal):
    exp = np.exp(sal)
    print(exp)
    print(np.sum(exp))
    scores = exp / np.sum(exp)
    return scores

inicio = time.time()
print(softmax(arr))
fin = time.time()
print(fin-inicio)