from re import M
import struct

def reverse(cadena):
    return cadena[::-1]

def binary(num,rev=1):
    if rev:
        return reverse(''.join('{:0>8b}'.format(c) for c in struct.pack('!f', num)))
    else:
        return ''.join('{:0>8b}'.format(c) for c in struct.pack('!f', num))

def binary(num,rev=1):
    if rev:
        return reverse(''.join('{:0>8b}'.format(num)))
    else:
        return ''.join('{:0>8b}'.format(c) for c in struct.pack('!f', num))

def convertFloatBin(m,rev=1):
    data_test=""
    for i in m:
        data_test+=binary(i,rev)
    return data_test

m = ['0.0343420691788196564']

d = convertFloatBin(m)
print(d)