from re import M
import struct
from decimal import *

from traceback2 import print_tb

def reverse(cadena):
    return cadena[::-1]

def binary(num,rev=1):
    if rev:
        return reverse(''.join('{:0>8b}'.format(c) for c in struct.pack('!f', num)))
    else:
        return ''.join('{:0>8b}'.format(c) for c in struct.pack('!f', num))

def convertFloatBin(m,rev=1):
    data_test=""
    for i in m:
        data_test+=binary(i,rev)
    return data_test

def convertFloatBin2(m,rev=1):
    data_test=[]
    for i in m:
        data_test.append(binary(i,rev))
    return data_test

def float_to_bin(value):  # For testing.
    """ Convert float to 64-bit binary string. """
    [d] = struct.unpack(">Q", struct.pack(">d", value))
    return '{:064b}'.format(d)

fileIn = open('data/outputMaxPooling2D_2.dat','r')

# print(line_f[0].)
# print(type(line_f[0]))
getcontext().prec=23
print(getcontext())
data=[]
line = fileIn.readline()
line_f = [Decimal(i) for i in line.split(',')]
print(line_f)
print(float_to_bin(line_f[2]))
# while line:
#     line_s = line.split(',')
#     print(line_s)
#     for dat in line_s:
#         data.append(float_to_bin(dat))
#     print(line_s)
#     break;
#     line = fileIn.readline()
