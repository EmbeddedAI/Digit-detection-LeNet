#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* Measure execution time */
#include <time.h>

#include <sys/time.h>

/**************************************************************************
*                            Constants Defintion                           *
**************************************************************************/
#define NUM_CLASS 10

/**************************************************************************
*                           Structure Defintion                           *
**************************************************************************/
typedef struct LayerInfo LayerInfo;
struct LayerInfo{
    char *name;
    char flagBias;
    int sizeBias;
    int sizeKernel;
};

/**************************************************************************
*                           Prototypes Functions                           *
**************************************************************************/
int chargeWeights(float***,float***,LayerInfo**);
int chargeDataTest(char*,float**,int,int,int);
void printData(float*,int,int,int,int);
char test(float*,float*,int,int,int,int);
/**************************************************************************
*                        Layers Prototypes Functions                       *              
**************************************************************************/
float* conv2DAddMatrix(float*,int,int,int,int);
float *sumChannels(float*,float*,float,int,int,int,int);
float *conv2DKernel(float*,float*,float*,int,int,int,int,int,int);
float* conv2D(float*,float*,float*,int,int,int,int,int,int,int,int);
float *maxPooling2D(float*,int,int,int,int,int);
float *flatten(float*,int,int,int);
float *dense(float*,float*,float*,int,int,int);
/**************************************************************************
*                  Activation Layers Prototypes Functions                  *
**************************************************************************/
float *reLU(float*,int,int,int);
float *softmax(float*,int);
int selectNumber(float*,int);

int main(int argc, char *argv[]){
    long long nanoseconds;
    // Start measuring time
    struct timespec begin, end; 
    struct timeval begin_s, end_s;
    int filters,channels,rows,columns,kernelSize,poolSize,stride,newColumns,newRows,inputNeurons,outputNeurons;
    char padding=1,activation,*filename="data/outputDense_2.dat",*filenameInput="data/number.dat";//*filename="data/outputFlatten.dat";
    float *data,**weights,**bias;
    int positionLayer,i,numLayers;
    LayerInfo *layersInfo;
    numLayers=chargeWeights(&weights,&bias,&layersInfo);
    // 0 // float dataInput[784] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.04313725605607,0.588235318660736,0.992156863212585,0.792156875133514,0.121568627655506,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.14509804546833,0.984313726425171,0.984313726425171,0.992156863212585,0.419607847929001,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.082352943718433,0.772549033164978,0.984313726425171,0.984313726425171,0.992156863212585,0.419607847929001,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.431372553110123,0.745098054409027,0.984313726425171,0.984313726425171,0.984313726425171,0.992156863212585,0.662745118141174,0.427450984716415,0.243137255311012,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.992156863212585,0.984313726425171,0.984313726425171,0.984313726425171,0.984313726425171,0.992156863212585,0.984313726425171,0.984313726425171,0.862745106220245,0.200000002980232,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.713725507259369,1,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.917647063732147,0.87058824300766,0.992156863212585,0.992156863212585,0.992156863212585,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.247058823704719,0.866666674613953,0.992156863212585,0.984313726425171,0.984313726425171,0.984313726425171,0.576470613479614,0.301960796117783,0.243137255311012,0.501960813999176,0.984313726425171,0.984313726425171,0.411764711141586,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.125490203499794,0.905882358551025,0.984313726425171,0.992156863212585,0.984313726425171,0.862745106220245,0.537254929542542,0.039215687662363,0,0,0.121568627655506,0.901960790157318,0.984313726425171,0.952941179275513,0.443137258291245,0.019607843831182,0,0,0,0,0,0,0,0,0,0,0,0,0.14509804546833,0.984313726425171,0.984313726425171,0.992156863212585,0.737254917621612,0.078431375324726,0,0,0,0,0,0.427450984716415,0.984313726425171,0.992156863212585,0.984313726425171,0.137254908680916,0,0,0,0,0,0,0,0,0,0,0,0,0.14509804546833,0.984313726425171,0.984313726425171,0.788235306739807,0.117647059261799,0,0,0,0,0,0,0.121568627655506,0.7843137383461,0.992156863212585,0.984313726425171,0.137254908680916,0,0,0,0,0,0,0,0,0,0,0,0,0.14509804546833,0.992156863212585,0.992156863212585,0,0,0,0,0,0,0,0,0.125490203499794,0.792156875133514,1,0.992156863212585,0.643137276172638,0,0,0,0,0,0,0,0,0,0,0,0,0.549019634723663,0.984313726425171,0.984313726425171,0,0,0,0,0,0,0,0,0.427450984716415,0.984313726425171,0.992156863212585,0.984313726425171,0.137254908680916,0,0,0,0,0,0,0,0,0,0,0,0,0.850980401039124,0.984313726425171,0.984313726425171,0,0,0,0,0,0,0.082352943718433,0.247058823704719,0.905882358551025,0.984313726425171,0.992156863212585,0.901960790157318,0.117647059261799,0,0,0,0,0,0,0,0,0,0,0,0,0.850980401039124,0.984313726425171,0.984313726425171,0,0,0,0,0,0,0.564705908298492,0.984313726425171,0.984313726425171,0.984313726425171,0.866666674613953,0.239215686917305,0,0,0,0,0,0,0,0,0,0,0,0,0,0.850980401039124,0.984313726425171,0.984313726425171,0,0,0,0,0,0.713725507259369,0.866666674613953,0.984313726425171,0.984313726425171,0.984313726425171,0.705882370471954,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.854901969432831,0.992156863212585,0.992156863212585,0.286274522542953,0.286274522542953,0.894117653369904,0.992156863212585,0.992156863212585,1,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.443137258291245,0.984313726425171,0.984313726425171,0.992156863212585,0.984313726425171,0.984313726425171,0.984313726425171,0.984313726425171,0.992156863212585,0.984313726425171,0.984313726425171,0.984313726425171,0.576470613479614,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.121568627655506,0.901960790157318,0.984313726425171,0.992156863212585,0.984313726425171,0.984313726425171,0.984313726425171,0.984313726425171,0.992156863212585,0.901960790157318,0.74117648601532,0.137254908680916,0.039215687662363,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.243137255311012,0.556862771511078,0.992156863212585,0.984313726425171,0.984313726425171,0.984313726425171,0.984313726425171,0.992156863212585,0.419607847929001,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.282352954149246,0.682352960109711,0.984313726425171,0.678431391716003,0.278431385755539,0.282352954149246,0.117647059261799,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    // 2 float dataInput[784] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.454901963472366,0.490196079015732,0.670588254928589,1,1,0.588235318660736,0.364705890417099,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.662745118141174,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.854901969432831,0.117647059261799,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.662745118141174,0.992156863212585,0.992156863212585,0.992156863212585,0.835294127464294,0.556862771511078,0.690196096897125,0.992156863212585,0.992156863212585,0.47843137383461,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.203921571373939,0.980392158031464,0.992156863212585,0.823529422283173,0.125490203499794,0.047058824449778,0,0.023529412224889,0.807843148708344,0.992156863212585,0.549019634723663,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.301960796117783,0.984313726425171,0.823529422283173,0.098039217293263,0,0,0,0.47843137383461,0.972549021244049,0.992156863212585,0.254901975393295,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.121568627655506,0.070588238537312,0,0,0,0,0.819607853889465,0.992156863212585,0.992156863212585,0.254901975393295,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.458823531866074,0.968627452850342,0.992156863212585,0.776470601558685,0.039215687662363,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.298039227724075,0.968627452850342,0.992156863212585,0.905882358551025,0.247058823704719,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.501960813999176,0.992156863212585,0.992156863212585,0.564705908298492,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.690196096897125,0.964705884456634,0.992156863212585,0.623529434204102,0.047058824449778,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.098039217293263,0.917647063732147,0.992156863212585,0.91372549533844,0.137254908680916,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.776470601558685,0.992156863212585,0.992156863212585,0.552941203117371,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.30588236451149,0.972549021244049,0.992156863212585,0.74117648601532,0.047058824449778,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.074509806931019,0.7843137383461,0.992156863212585,0.992156863212585,0.552941203117371,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.52549022436142,0.992156863212585,0.992156863212585,0.678431391716003,0.047058824449778,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.972549021244049,0.992156863212585,0.992156863212585,0.098039217293263,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.972549021244049,0.992156863212585,0.992156863212585,0.168627455830574,0.078431375324726,0.078431375324726,0.078431375324726,0.078431375324726,0.019607843831182,0,0.019607843831182,0.078431375324726,0.078431375324726,0.14509804546833,0.588235318660736,0.588235318660736,0.588235318660736,0.576470613479614,0.039215687662363,0,0,0,0,0,0,0,0,0,0.972549021244049,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.658823549747467,0.560784339904785,0.650980412960053,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.482352942228317,0,0,0,0,0,0,0,0,0,0.682352960109711,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.992156863212585,0.976470589637756,0.968627452850342,0.968627452850342,0.662745118141174,0.458823531866074,0.458823531866074,0.223529413342476,0,0,0,0,0,0,0,0,0,0,0.462745100259781,0.482352942228317,0.482352942228317,0.482352942228317,0.650980412960053,0.992156863212585,0.992156863212585,0.992156863212585,0.607843160629272,0.482352942228317,0.482352942228317,0.16078431904316,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    // 7 //
            float dataInput[784] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.329411774873734,0.725490212440491,0.623529434204102,0.592156887054443,0.235294118523598,0.141176477074623,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.87058824300766,0.996078431606293,0.996078431606293,0.996078431606293,0.996078431606293,0.945098042488098,0.776470601558685,0.776470601558685,0.776470601558685,0.776470601558685,0.776470601558685,0.776470601558685,0.776470601558685,0.776470601558685,0.666666686534882,0.203921571373939,0,0,0,0,0,0,0,0,0,0,0,0,0.26274511218071,0.447058826684952,0.282352954149246,0.447058826684952,0.639215707778931,0.890196084976196,0.996078431606293,0.882352948188782,0.996078431606293,0.996078431606293,0.996078431606293,0.980392158031464,0.898039221763611,0.996078431606293,0.996078431606293,0.549019634723663,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.066666670143604,0.258823543787003,0.054901961237192,0.26274511218071,0.26274511218071,0.26274511218071,0.23137255012989,0.082352943718433,0.925490200519562,0.996078431606293,0.415686279535294,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.325490206480026,0.992156863212585,0.819607853889465,0.070588238537312,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.086274512112141,0.91372549533844,1,0.325490206480026,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.505882382392883,0.996078431606293,0.933333337306976,0.172549024224281,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.23137255012989,0.976470589637756,0.996078431606293,0.243137255311012,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.521568655967712,0.996078431606293,0.733333349227905,0.019607843831182,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.035294119268656,0.803921580314636,0.972549021244049,0.227450981736183,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.494117647409439,0.996078431606293,0.713725507259369,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.294117659330368,0.984313726425171,0.941176474094391,0.223529413342476,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.074509806931019,0.866666674613953,0.996078431606293,0.650980412960053,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.011764706112444,0.796078443527222,0.996078431606293,0.858823537826538,0.137254908680916,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.149019613862038,0.996078431606293,0.996078431606293,0.301960796117783,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.121568627655506,0.878431379795074,0.996078431606293,0.450980395078659,0.003921568859369,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.521568655967712,0.996078431606293,0.996078431606293,0.203921571373939,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.239215686917305,0.949019610881805,0.996078431606293,0.996078431606293,0.203921571373939,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.474509805440903,0.996078431606293,0.996078431606293,0.858823537826538,0.156862750649452,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.474509805440903,0.996078431606293,0.811764717102051,0.070588238537312,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    // float *dataInput;
    // chargeDataTest(filenameInput,&dataInput,1,28,28);
    filters=20;channels=1;rows=28;columns=28;kernelSize=5;activation=1;stride=1;
    //printf("Data\n");
    //printData(dataInput,1,channels,rows,columns);
    //printf("Bias\n");
    //printData(bias[2],1,1,1,500);
    //printf("Kernel\n");
    //printData(weights[2],1,1,500,2450);
    clock_gettime(CLOCK_REALTIME, &begin);
    gettimeofday(&begin_s, 0);
    data = conv2D(dataInput,bias[0],weights[0],filters,channels,rows,columns,kernelSize,activation,stride,padding);
    poolSize=2;stride=2;
    data = maxPooling2D(data,filters,rows,columns,poolSize,stride);
    channels=filters;filters=50;columns=(columns-poolSize)/stride+1;rows=(rows-poolSize)/stride+1;kernelSize=5;activation=1;stride=1;
    data = conv2D(data,bias[1],weights[1],filters,channels,rows,columns,kernelSize,activation,stride,padding);
    stride=2;
    data = maxPooling2D(data,filters,rows,columns,poolSize,stride);
    columns=(columns-poolSize)/stride+1;rows=(rows-poolSize)/stride+1;
    printf("Columns: %d, Rows: %d, Filters: %d, InputNeurons: %d\n",columns,rows,filters,columns*rows*filters);
    data=flatten(data,filters,rows,columns);
    inputNeurons=columns*rows*filters;outputNeurons=500;columns=outputNeurons;rows=1;filters=1;
    //printf("InputNeurons = %d,  OutputNeurons = %d\n",inputNeurons,outputNeurons);
    data = dense(data,bias[2],weights[2],inputNeurons,outputNeurons,activation);
    // float biasDenseTwo[2] = {0.031813578694834,-0.016992205753923};
    // float kernelDenseTwo[6] = {0.106762535870075,0.017809865996242,-0.056545231491327,0.108123563230038,0.116251200437546,-0.115510806441307};
    inputNeurons=outputNeurons;outputNeurons=NUM_CLASS;columns=outputNeurons;activation=2;
    printf("InputNeurons = %d,  OutputNeurons = %d\n",inputNeurons,outputNeurons);
    data = dense(data,bias[3],weights[3],inputNeurons,outputNeurons,activation);
    clock_gettime(CLOCK_REALTIME, &end);
     gettimeofday(&end_s, 0);
    /* TEST RESULT */
    printf("Result: ");
    //printData(data,filters,1,rows,columns);
    printData(data,1,1,1,outputNeurons);
    printf("Rows: %d  Columns: %d, Filters: %d\n",rows,columns,filters);
    float *dataTest;
    chargeDataTest(filename,&dataTest,1,1,outputNeurons);
    chargeDataTest(filename,&dataTest,filters,rows,columns);
    if(test(data,dataTest,1,1,1,outputNeurons))
        printf("Error\n");
    else
        printf("OK\n");
    printf("Number Recognized: %d\n",selectNumber(data,NUM_CLASS));

    nanoseconds = end.tv_sec - begin.tv_sec;
    printf("Time (ns): %lld\n",nanoseconds);
    long seconds = end_s.tv_sec - begin_s.tv_sec;
    long microseconds = end_s.tv_usec - begin_s.tv_usec;
    double elapsed = seconds + microseconds*1e-6;
    
    printf("Time measured: %.6f seconds.\n", elapsed);
    return 0;
} // end main

/**************************************************************************
*   Function:   chargeWeights()
*   Purpose:    This is the function for charge weights and parameters 
*               for layers in the CNN model.
*   Arguments:
*       weightsP:       Array passed by reference to store the kernels 
*                       weights of the layers in the CCN model.
*       biasP:          Array passed by reference to store the bias 
*                       weights of the layers in the CCN model.
*       layersInfoP:    Array with the information of layers on the 
*                       CNN model.
*   Return:
*       Number of layers with weights in the CNN.
**************************************************************************/
int chargeDataTest(char *filename, float**dataTestP, int filters, int rows, int columns){
    int i;
    float *dataTest=(float*)calloc(filters*rows*columns,sizeof(float)),*ptrDataTest=&dataTest[0];
    char *contents=NULL,*token=NULL,*ptrTrash;
    FILE *fp=fopen(filename,"r");
    if (!fp){
        printf("El archivo %s no se puede abrir o no existe\n",filename);
        exit(EXIT_FAILURE);
    }   // end if error open file filename
    size_t len=0;
    while (getline(&contents, &len, fp) != -1){
        token=strtok(contents,",");
        for(i=0;i<columns;++i){
            *ptrDataTest=strtold(token,&ptrTrash);
            ++ptrDataTest;
            token=strtok(NULL,",");
        }
    } // end file weights.cfg
    fclose(fp);// close the file
    *dataTestP=dataTest;
    return 1;
} // end chargeDataTest

/**************************************************************************
*   Function:   chargeWeights()
*   Purpose:    This is the function for charge weights and parameters 
*               for layers in the CNN model.
*   Arguments:
*       weightsP:       Array passed by reference to store the kernels 
*                       weights of the layers in the CCN model.
*       biasP:          Array passed by reference to store the bias 
*                       weights of the layers in the CCN model.
*       layersInfoP:    Array with the information of layers on the 
*                       CNN model.
*   Return:
*       Number of layers with weights in the CNN.
**************************************************************************/
int chargeWeights(float***weightsP, float ***biasP, LayerInfo **layersInfoP){
    float **weights,**bias,*ptrWeights,*ptrBias;
    char *filename="Conf/weights.cfg",*filenameBias="Conf/weightsBias.cfg",*filenameKernel="Conf/weightsKernel.cfg",flagBias,flagBiasGeneral,*contents=NULL,*token=NULL,*ptrTrash;
    int i,numLayers,positionLayer=0;
    LayerInfo *layersInfo;
    FILE *fp=fopen(filename,"r");
    if (!fp){
        printf("El archivo %s no se puede abrir o no existe\n",filename);
        exit(EXIT_FAILURE);
    }   // end if error open file filename
    size_t len=0;
    getline(&contents, &len, fp);
    token=strtok(contents,",");
    numLayers=atoi(token);
    token=strtok(NULL,",");
    flagBiasGeneral=atoi(token);
    weights=(float**)calloc(numLayers,sizeof(float*));
    layersInfo=(LayerInfo*)calloc(numLayers,sizeof(LayerInfo));
    if(flagBiasGeneral)
        bias=(float**)calloc(numLayers,sizeof(float*));
    printf("NumLayers: %d, FlagBias: %d\n",numLayers,flagBiasGeneral);
    while (getline(&contents, &len, fp) != -1){
        token=strtok(contents,",");
        layersInfo[positionLayer].name = (char*)calloc(strlen(token),sizeof(char));
        strcpy(layersInfo[positionLayer].name,token);
        token=strtok(NULL,",");
        flagBias=atoi(token);
        layersInfo[positionLayer].flagBias=flagBias;
        if(flagBias){
            token=strtok(NULL,",");
            layersInfo[positionLayer].sizeBias=atoi(token);
            bias[positionLayer]=(float*)calloc(layersInfo[positionLayer].sizeBias,sizeof(float));
            printf("SizeArray Bias: %d\n",layersInfo[positionLayer].sizeBias);
        }else{
            layersInfo[positionLayer].sizeBias=0;
        }
        token=strtok(NULL,",");
        layersInfo[positionLayer].sizeKernel=atoi(token);
        weights[positionLayer]=(float*)calloc(layersInfo[positionLayer].sizeKernel,sizeof(float));
        printf("SizeArray Kernel: %d\n",layersInfo[positionLayer].sizeKernel);
        ++positionLayer;
    } // end file weights.cfg
    positionLayer=0;
    fclose(fp);// close the file
    if(flagBiasGeneral){
        fp=fopen(filenameBias,"r");
        if (!fp){
            printf("El archivo %s no se puede abrir o no existe\n",filenameBias);
            exit(EXIT_FAILURE);
        } // end if error open file filenameBias
        for(positionLayer=0;positionLayer<numLayers;++positionLayer){
            getline(&contents, &len, fp);
            token=strtok(contents,",");
            ptrBias = bias[positionLayer];
            for(i=0;i<layersInfo[positionLayer].sizeBias;++i){
                *ptrBias=strtold(token,&ptrTrash);
                ++ptrBias;
                token=strtok(NULL,",");
            } // end for i: layersInfo[positionLayer].sizeBias
        } // end for positionLayer: numLayers
        fclose(fp);// close the file of bias weights
    } // end if flagBiasGeneral
    fp=fopen(filenameKernel,"r");
    if (!fp){
        printf("El archivo %s no se puede abrir o no existe\n",filenameKernel);
        exit(EXIT_FAILURE);
    } // end if error open file filenameKernel
    for(positionLayer=0;positionLayer<numLayers;++positionLayer){
        getline(&contents, &len, fp);
        //printf("%s\n",contents);
        token=strtok(contents,",");
        ptrWeights = weights[positionLayer];
        for(i=0;i<layersInfo[positionLayer].sizeKernel;++i){
            *ptrWeights=strtold(token,&ptrTrash);
            ++ptrWeights;
            token=strtok(NULL,",");
        } // end for i: layersInfo[positionLayer].sizeKernel
    } // end for positionLayer: numLayers
    fclose(fp);// close the file of kernel weights
    *weightsP=weights;
    *biasP=bias;
    *layersInfoP=layersInfo;
    return numLayers;
} // end chargeWeights

/**************************************************************************
*   Function:   printData()
*   Purpose:    This is the function for test this layer.
*   Arguments:
*       data:       Array with the output data of this layer.
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       void
**************************************************************************/
void printData(float* data, int filters, int channels, int rows, int columns){
    int filterPostion,channelPosition,columnPosition,rowPosition,dataPosition=0;
    for(filterPostion = 0;filterPostion<filters;++filterPostion){
        for(channelPosition = 0;channelPosition<channels;++channelPosition){
            printf(" {");
            for(columnPosition = 0;columnPosition<rows;++columnPosition){
                printf(" [");
                for(rowPosition = 0;rowPosition<columns;++rowPosition){
                    printf("%.19f ",data[dataPosition]);
                    dataPosition++;
                } // end for rowPosition: newColumns
                printf("]\n");
            } // end for columnPosition: newRows
            printf("}\n");
        } // end for channelPosition: channels
        printf("\n");
        printf("\n");
    } // end for filterPostion: filters
} // end printData

/**************************************************************************
*   Function:   selectNumber()
*   Purpose:    This function select the number that is recognized by the
*               CNN model.
*   Arguments:
*       result:     Array with the output data of softmax layer.
*       numClasses:    Number of classes for the output of CNN model.
*   Return:
*       Number selected by the softmax layer in the output CNN.
**************************************************************************/
int selectNumber(float* result,int numClasses){
    int i,select;
    float max = 0;
    for(i=0;i<numClasses;++i){
        if(*result > max){
            max = *result;
            select = i;
        } // end if max
        ++result;
    } // end for i
    return select;
} // end selectNumber


/**************************************************************************
*   Function:   test()
*   Purpose:    This is the function for test this layer.
*   Arguments:
*       data:       Array with the output data of this layer.
*       dataTest:   Array with the expected output data of this layer
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       Boolean for indicate if the result layer is Error or Correct.
*       1 - Error
*       0 - Correct
**************************************************************************/
char test(float *data, float *dataTest, int filters, int channels, int columns, int rows){
    //float comparator = 0.0000007; // conv2D_1 maxPooling2D_1
    //float comparator = 0.0000002; // conv2D_2 maxPooling2D_2
    float comparator = 0.00000006;
    for(int i = 0; i<filters*channels*columns*rows;++i){
        if(fabsf(dataTest[i]-data[i])>comparator){
            printf("Error in: dataTest[%d] = %.19f  data[%d] = %.19f Rest= %.19f \n",i,dataTest[i],i,data[i],fabsf(dataTest[i]-data[i]));
            return 1;
        }
    }
    return 0;
} // end test

/**************************************************************************
 *                                 Layers                                 *
 **************************************************************************/
/**************************************************************************
*   Function:   conv2DAddMatrix()
*   Purpose:    This is the first block of Conv2D  is in charge of 
*               handling the padding in the convolution, what it does is 
*               add columns and rows around the matrix according to the 
*               following equations.
*   Arguments:
*       data:       Array with the input data of this Conv2D layer.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       cols:       Number of cols of each channel in the data array.
*       kernelSize: Kernel size of this Conv2D layer.
*   Return:
*       Array with output of this block from the Conv2D.
**************************************************************************/
float* conv2DAddMatrix(float *data, int channels, int rows, int columns, int kernelSize){
    int padding=kernelSize-1,padding2=padding/2,newColumns=columns+padding,newRows=rows+padding,dataNewPosition=padding2*(newColumns+1),columnPosition=padding2,rowPosition=padding2;
    char flagZero, flagBegin=1;
    float *dataNew=(float*)calloc(channels*newRows*newColumns,sizeof(float)),*ptrDataNew=&dataNew[0]+padding2*(newColumns+1),*ptrData=&data[0];
    while(dataNewPosition<(channels*newRows*newColumns)-padding2*(newColumns+1)){
        *ptrDataNew=*ptrData;
        ++ptrData;
        rowPosition++;
        if(rowPosition==columns+padding2){
            rowPosition=padding2;
            columnPosition++;
            if(columnPosition==rows+padding2){
                columnPosition=padding2;
                ptrDataNew+=(padding*(newColumns+1))+1;
                dataNewPosition+=(padding*(newColumns+1))+1;
            }else{
                ptrDataNew+=padding+1;
                dataNewPosition+=padding+1;
            }
        }else{
            ++ptrDataNew;
            dataNewPosition++;
        }
    } // end for dataNewPosition: data_new
    return dataNew;
} // end conv2DAddMatrix

/**************************************************************************
*   Function:   sumChannels()
*   Purpose:    Sum by each position of the three channel for obtain one
*               array with the result of convolution of one filter.
*   Arguments:
*       data:           Array with the ouput data of this Conv2D layer.
*       dataFilter:     Array with convolution calculate by each channel.
*       bias:           Bias of this filter.
*       channels:       Number of channels of each filter in the data array.
*       startFilter:    Position of the filter in data array.
*       rows:           Number of rows of each filter in the data array.
*       columns:        Number of cols of each channel in the data array.
*   Return:
*       Array with sum of three channels by each position.
**************************************************************************/
float *sumChannels(float *data, float *dataFilter, float bias, int channels, int startFilter, int rows, int columns){
    float *ptrData=&data[startFilter], **ptrChannels = (float**)calloc(channels,sizeof(float*)), **ptrBeginChannels=&ptrChannels[0],sumAux = 0;
    int channelPosition,dataPosition;
    for(channelPosition=0;channelPosition<channels;++channelPosition){
        *ptrChannels = &dataFilter[channelPosition*rows*columns];
        ++ptrChannels;
    }
    ptrChannels = ptrBeginChannels;
    for(dataPosition=0;dataPosition<rows*columns;++dataPosition){
        for(channelPosition=0;channelPosition<channels;++channelPosition){
            sumAux += **ptrChannels;
            ++*ptrChannels;
            ++ptrChannels;
        }
        ptrChannels = ptrBeginChannels;
        *ptrData = sumAux + bias;
        sumAux = 0;
        ++ptrData;
    }
    return data;
} // end sumChannels

/**************************************************************************
*   Function:   conv2DKernel()
*   Purpose:    This is the second block of Conv2D  is in charge of 
*               performing the convolution with the kernels of the input
*               data.
*   Arguments:
*       data:       Array with the input data of this Conv2D layer.
*       kernel:     Array with the weights of the kernels of this layer.
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       cols:       Number of cols of each channel in the data array.
*       kernelSize: Kernel size of this Conv2D layer.
*   Return:
*       Array with output of this block from the Conv2D.
**************************************************************************/
float *conv2DKernel(float *data, float *bias, float *kernel, int filters, int channels, int rows, int columns, int kernelSize, int stride){
    int timesKernel,kernelRowsPosition=0,kernelColumnsPosition=0,rowPosition=0,columnPosition=0,newCols=(columns-kernelSize)/stride+1,newRows=(rows-kernelSize)/stride+1,kernelTop = kernelSize*kernelSize;
    int channelPosition = 0,filterPosition=0;
    float *ptrData = &data[0], *ptrBias = &bias[0], *ptrKernel = &kernel[0], *ptrColumnsBeginData = &data[0],*ptrBeginData = &data[0], *ptrBeginKernel = &kernel[0],sumProduct=0;
    float *dataNew = (float*)calloc(filters*newCols*newRows,sizeof(float)), *ptrDataNew = &dataNew[0];
    float *dataFilter = (float*)calloc(channels*newCols*newRows,sizeof(float)), *ptrDataFilter = &dataFilter[0];
    for(timesKernel=0;timesKernel<=filters*channels*newCols*newRows*kernelTop;++timesKernel){
        if(rowPosition+kernelSize-1 > columns-1){
            rowPosition = 0;
            kernelRowsPosition = 0;
            kernelColumnsPosition = 0;
            ptrData = ptrColumnsBeginData + stride*columns;
            ptrColumnsBeginData = ptrData;
            ptrBeginData = ptrData;
            columnPosition += stride;
            if(columnPosition+kernelSize-1 > rows-1){
                columnPosition = 0;
                ++channelPosition;
                ptrData = ptrColumnsBeginData + (kernelSize-1)*columns;
                ptrColumnsBeginData = ptrData;
                ptrBeginData = ptrData;
                ptrKernel = ptrBeginKernel + kernelTop;
                ptrBeginKernel = ptrKernel;
                if(channelPosition == channels){
                    channelPosition = 0;
                    if(channels > 1)
                        dataNew = sumChannels(dataNew,dataFilter,*ptrBias,channels,filterPosition*newRows*newCols,newRows,newCols);
                    ++ptrBias;
                    ++filterPosition;
                    ptrData = &data[0];
                    ptrColumnsBeginData = &data[0];
                    ptrBeginData = &data[0];
                    ptrDataFilter = &dataFilter[0];
                }
            }else{
                ptrKernel = ptrBeginKernel;
            } // end if columnPosition: rows
        }// end if rowPosition: columns
        sumProduct += (*ptrData**ptrKernel);//(data[dataPostion]*kernel[kernelPosition])
        ++ptrKernel;
        ++kernelRowsPosition;
        ++ptrData;
        if(kernelRowsPosition == kernelSize){
            kernelRowsPosition = 0;
            ptrData += (columns-kernelSize);
            ++kernelColumnsPosition;
            if(kernelColumnsPosition==kernelSize){
                kernelColumnsPosition = 0;
                rowPosition += stride;
                if(channels == 1){
                    *ptrDataNew = (sumProduct+*ptrBias);
                    ++ptrDataNew;
                }else{
                    *ptrDataFilter = sumProduct;
                    ++ptrDataFilter;
                }
                sumProduct = 0;
                ptrData = ptrBeginData+stride;
                ptrBeginData = ptrData;
                ptrKernel = ptrBeginKernel;
            } // end if kernelColumnsPosition: kernelSize
        } // end if kernelRowsPosition: kernelSize
    } // end for timesKernel: filters*channels*newCols*newRows*kernelTop
    return dataNew;
} // end conv2DKernel

float* conv2D(float *data, float *bias, float *kernel, int filters, int channels, int rows, int columns, int kernelSize, int stride, int activation, int flagPadding){
    int padding = kernelSize-1,paddingColumns = columns+padding,paddingRows = rows+padding;
    if(activation && flagPadding)
        return reLU(conv2DKernel(conv2DAddMatrix(data,channels,rows,columns,kernelSize),bias,kernel,filters,channels,paddingRows,paddingColumns,kernelSize,stride),filters, rows, columns);
    else if(activation && !flagPadding)
        return reLU(conv2DKernel(data,bias,kernel,filters,channels,rows,columns,kernelSize,stride),filters, rows, columns);
    else if(!activation && flagPadding)
        return conv2DKernel(conv2DAddMatrix(data,channels,rows,columns,kernelSize),bias,kernel,filters,channels,paddingRows,paddingColumns,kernelSize,stride);
    else 
        return conv2DKernel(data,bias,kernel,filters,channels,rows,columns,kernelSize,stride);        
} // end conv2D

/**************************************************************************
*   Function:   maxPooling2D()
*   Purpose:    The maximum of the positions covered by the pool is taken,
*               which is of size poolSize x poolSize, this pool runs
*               through the entire matrix jumping in steps defined in the
*               stride, until it obtains a new matrix with all the maximums,
*               this being smaller than the original by equation 1 and 2
*               that we see in the image.
*   Arguments:
*       data:       Array with the input data of this Conv2D layer.
*       filters:    Number of filters in the data array
*       rows:       Number of rows of each channel in the data array.
*       cols:       Number of cols of each channel in the data array.
*       poolSize:   Pool size of this Conv2D layer.
*   Return:
*       Array with output of this layer MaxPooling2D.
**************************************************************************/
float *maxPooling2D(float *data, int filters, int rows, int columns, int poolSize, int stride){
    int timesKernel,poolRowsPosition=0,poolColumnsPosition=0,rowPosition=0,columnPosition=0,newColumns=(columns-poolSize)/stride+1,newRows=(rows-poolSize)/stride+1,poolTop = poolSize*poolSize;
    float *ptrData = &data[0], *ptrColumnsBeginData = &data[0],*ptrBeginData = &data[0], maxComparator = -10000;
    float *dataNew = (float*)calloc(filters*newColumns*newRows,sizeof(float)), *ptrDataNew = &dataNew[0];
    int dataPosition=0,beginDataPosition=0,columnsBeginDataPosition=0;
    for(timesKernel=0;timesKernel<=filters*newColumns*newRows*poolTop;++timesKernel){
        if(rowPosition+poolSize-1 > columns-1){
            //printf("%d == %d",rowPosition+poolSize-1,columns);
            rowPosition = 0;
            poolRowsPosition = 0;
            poolColumnsPosition = 0;
            columnPosition +=stride;
            if(columnPosition+poolSize-1 > rows+-1){
                ptrData = ptrColumnsBeginData+(columns-columnPosition+stride)*columns;
                columnPosition = 0;
                ptrColumnsBeginData = ptrData;
                ptrBeginData = ptrData;
                // test //
                dataPosition = columnsBeginDataPosition+poolSize*columns;
                columnsBeginDataPosition = dataPosition;
                beginDataPosition = dataPosition;
                // test //
            }else{
                ptrData = ptrColumnsBeginData + stride*columns;
                ptrColumnsBeginData = ptrData;
                ptrBeginData = ptrData;
                // test //
                dataPosition = columnsBeginDataPosition + stride*columns;
                columnsBeginDataPosition = dataPosition;
                beginDataPosition = dataPosition;
                // test //
            }// end if columnPosition: rows
        }// end if rowPosition: column
        if(*ptrData > maxComparator)
            maxComparator = *ptrData;
        //printf("Dataposition: %d | Data: %f\n",dataPosition,*ptrData);
        ++poolRowsPosition;
        ++ptrData;
        // test //
        ++dataPosition;
        // test //
        if(poolRowsPosition == poolSize){
            poolRowsPosition = 0;
            ptrData += (columns-poolSize);
            // test //
            dataPosition += (columns-poolSize);
            // test //
            ++poolColumnsPosition;
            if(poolColumnsPosition==poolSize){
                poolColumnsPosition = 0;
                rowPosition += stride;
                *ptrDataNew = maxComparator;
                ++ptrDataNew;
                maxComparator = -10000;
                ptrData = ptrBeginData+stride;
                ptrBeginData = ptrData;
                // test //
                dataPosition = beginDataPosition+stride;
                beginDataPosition = dataPosition;
                // test //
            } // end if poolColumnsPosition: poolSize
        } // end if poolRowsPosition: poolSize
    } // end for timesKernel: filters*channels*newColumns*newRows*poolTop
    return dataNew;
} // end maxPooling2D

/**************************************************************************
*   Función:   flatten()
*   Proposito:  Performs the transposition that performs the Flatten 
*               layer in Python Keras
*   Argumentos:
*       data:   Array with the input data of this layer.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Retorno:
*       Array with output of this layer
**************************************************************************/
float *flatten(float *data, int channels, int rows, int columns){
    int dataPosition,channelPosition=0,columnPosition=0,rowPosition=0;
    float *dataFlatten=(float*)calloc(channels*rows*columns,sizeof(float));
    for(dataPosition=0;dataPosition<channels*rows*columns;++dataPosition){
        dataFlatten[rowPosition*rows*channels+channelPosition*rows+columnPosition]=data[channelPosition*rows*columns+columnPosition*columns+rowPosition];
        ++rowPosition;
        if(rowPosition==columns){
            rowPosition=0;
            ++columnPosition;
            if(columnPosition==rows){
                columnPosition=0;
                ++channelPosition;
            } // end if rows
        } // end if columns
    } // end for dataFlatten
    return dataFlatten;
} // end flatten

/**************************************************************************
*   Función:   dense()
*   Proposito:  Dense Layer that is one layer of the fully connected
*   Argumentos:
*       data:   Array with the input data of this layer.
*       bias:   Array with bias of dense layer.
*       kernel: Array with weights of the connection of input neurons and
*               neurons of this layer.
*       inputNeurons:   Number of neurons from previous layer.
*       neurons:        Number of neurons in this layer.
*   Retorno:
*       Array with output of this layer
**************************************************************************/
float *dense(float *data, float *bias, float *kernel, int inputNeurons, int neurons,int activation){
    int weightsPosition,dataPosition=0;
    float *dataNew= (float*)calloc(neurons,sizeof(float)),sumProduct=0,*ptrData=&data[0],*ptrBeginData=&data[0],*ptrBias=&bias[0],*ptrKernel=&kernel[0],*ptrDataNew=&dataNew[0];
    for(weightsPosition=0;weightsPosition<neurons*inputNeurons;++weightsPosition){
        sumProduct+=(*ptrKernel**ptrData);
        ++ptrKernel;
        ++dataPosition;
        ++ptrData;
        if(dataPosition==inputNeurons){
            dataPosition=0;
            ptrData=ptrBeginData;
            sumProduct+=*ptrBias;
            *ptrDataNew=sumProduct;
            sumProduct=0;
            ++ptrBias;
            ++ptrDataNew;
        } // if dataPosition: data
    } // for weightsPosition: kernel
    if(activation==2)
        return softmax(dataNew,NUM_CLASS);
    else if(activation==1)
        return reLU(dataNew,1,1,neurons);
    else
        return dataNew;
} // end Dense

/**************************************************************************
 *                           Activation Layers                            *
 **************************************************************************/
/**************************************************************************
*   Function:   reLU()
*   Purpose:  Activation Layer ReLU
*   Arguments:
*       data:       Array with the input data of this activation layer.
*       filters:    Number of filters in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       Array with negative data converted to 0 and positive data equal 
*       to input.
**************************************************************************/
float *reLU(float *data, int filters, int rows, int columns){
    int i;
    float *ptrData = &data[0];
    for(i=0;i<filters*rows*columns;++i){
        if(*ptrData<0)
            *ptrData=0;
        ++ptrData;
    } // for i:data
    return data;
} // end reLU

/**************************************************************************
*   Function:   softmax()
*   Purpose:  Softmax Layer ReLU
*   Arguments:
*       output_fully_connected: Array with the output data of 
*                               fully connected.
*   Return:
*       Array with output data of activation layer
**************************************************************************/
float *softmax(float* input, int numClasses){
    int i;
    float sum_array = 0,*ptrInput = &input[0];
    for(i = 0;i<numClasses; ++i){
        *ptrInput = exp(*ptrInput);
        sum_array += *ptrInput;
        ++ptrInput;
    } // end if
    ptrInput=&input[0];
    for(i = 0;i<numClasses; ++i){
        *ptrInput /= sum_array;
        ++ptrInput;
    } // i: NUM_CLASS
    return input;
} // end softmax