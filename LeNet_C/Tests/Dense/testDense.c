#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/**************************************************************************
*                            Constans Defintion                           *
**************************************************************************/
#define NUM_CLASS 10
/**************************************************************************
*                           Protoypes Functions                           *
**************************************************************************/
void printData(long double*,int,int,int,int);
char test(long double*,long double*,int,int,int,int);
/**************************************************************************
*                        Layers Protoypes Functions                       *              
**************************************************************************/
long double *dense(long double*,long double*,long double*,int,int,int);
/**************************************************************************
*                  Activation Layers Protoypes Functions                  *
**************************************************************************/
long double *reLU(long double*,int,int,int);
long double *softmax(long double*,int);

int main(){
    int inputNeurons=5,outputNeurons=3,activation=1;
    long double dataInput[5] = {1173.0,1185.0,1197.0,1209.0,1215.0};
    long double kernel[15] = {-0.759395137636368000,-0.978103060974112000,0.998121683501480000,-0.325861252358555000,-0.765074163991697000,0.839301826606772000,-0.883896114128242000,0.159234027089666000,0.562705258016950000,0.590438703784899000,0.461052517505008000,0.737630417639106000,0.725025516392781000,-0.571476848550602000,0.936026200657134000};
    long double bias[3] = {0.807088733759321000,-0.673662658940427000,0.890690586367314000};
    long double dataTest[3] = {0,1524.707297176310000000,2730.009205544980000000};
    //long double kernel[15] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    //long double bias[3] = {3,2,1};
    //long double dataTest[3] = {18048,47942,77836};
    printf("Data\n");
    printData(dataInput,1,1,1,inputNeurons);
    printf("Kernel\n");
    printData(kernel,1,1,1,inputNeurons*outputNeurons);
    long double *data = dense(dataInput,bias,kernel,inputNeurons,outputNeurons,activation);
    printf("Result: ");
    if(test(data,dataTest,1,1,1,outputNeurons))
        printf("Error\n");
    else
        printf("OK\n");
    printData(data,1,1,1,outputNeurons);
    return 0;
} // end main

/**************************************************************************
*   Function:   printData()
*   Purpose:    This is the function for test this layer.
*   Arguments:
*       data:       Array with the output data of this layer.
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       void
**************************************************************************/
void printData(long double* data, int filters, int channels, int rows, int columns){
    int filterPostion,channelPosition,columnPosition,rowPosition,dataPosition=0;
    for(filterPostion = 0;filterPostion<filters;++filterPostion){
        for(channelPosition = 0;channelPosition<channels;++channelPosition){
            for(columnPosition = 0;columnPosition<rows;++columnPosition){
                printf(" ");
                for(rowPosition = 0;rowPosition<columns;++rowPosition){
                    printf("%.18Lf ",data[dataPosition]);
                    dataPosition++;
                } // end for rowPosition: newColumns
                printf("\n");
            } // end for columnPosition: newRows
            printf("\n");
        } // end for channelPosition: channels
        printf("\n");
        printf("\n");
    } // end for filterPostion: filters
} // end printData

/**************************************************************************
*   Function:   test()
*   Purpose:    This is the function for test this layer.
*   Arguments:
*       data:       Array with the output data of this layer.
*       dataTest:   Array with the expected output data of this layer
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       Boolean for indicate if the result layer is Error or Correct.
*       1 - Error
*       0 - Correct
**************************************************************************/
char test(long double *data, long double *dataTest, int filters, int channels, int columns, int rows){
    long int comparator = 1000000000000000000;
    for(int i = 0; i<filters*channels*columns*rows;++i)
        if((long int)(dataTest[i]*comparator) != (long int)(data[i]*comparator))//if((long int)(dataTest[i]*t) != (long int)(data[i]*t))
            return 1;
    return 0;
} // end test

/**************************************************************************
 *                                 Layers                                 *
 **************************************************************************/
/**************************************************************************
*   Función:   dense()
*   Proposito:  Dense Layer that is one layer of the fully connected
*   Argumentos:
*       data:   Array with the input data of this layer.
*       bias:   Array with bias of dense layer.
*       kernel: Array with weights of the connection of input neurons and
*               neurons of this layer.
*       inputNeurons:   Number of neurons from previous layer.
*       neurons:        Number of neurons in this layer.
*   Retorno:
*       Array with output of this layer
**************************************************************************/
long double *dense(long double *data, long double *bias, long double *kernel, int inputNeurons, int neurons,int activation){
    int weightsPosition,dataPosition=0;
    long double *dataNew= (long double*)calloc(neurons,sizeof(long double));
    long double sumProduct=0, *ptrData = &data[0], *ptrBeginData = &data[0], *ptrBias = &bias[0], *ptrKernel = &kernel[0], *ptrDataNew = &dataNew[0];
    for(weightsPosition=0; weightsPosition<neurons*inputNeurons;++weightsPosition){
        sumProduct += (*ptrKernel**ptrData);
        //printf("Sumproduct: %.18f * %.18f = %.18f\n",*ptrKernel,*ptrData,sumProduct);
        ++ptrKernel;
        ++dataPosition;
        ++ptrData;
        if(dataPosition==inputNeurons){
            dataPosition=0;
            ptrData = ptrBeginData;
            sumProduct += *ptrBias;
            //printf("Sumproduct: + %.18f = %.18f\n",*ptrBias,sumProduct);
            *ptrDataNew = sumProduct;
            sumProduct = 0;
            ++ptrBias;
            ++ptrDataNew;
        } // if dataPosition: data
    } // for weightsPosition: kernel
    if(activation == 2)
        return softmax(dataNew,NUM_CLASS);
    else if(activation == 1)
        return reLU(dataNew,1,1,neurons);
    else
        return dataNew;
} // end Dense

/**************************************************************************
 *                           Activation Layers                            *
 **************************************************************************/
/**************************************************************************
*   Function:   reLU()
*   Purpose:  Activation Layer ReLU
*   Arguments:
*       data:       Array with the input data of this activation layer.
*       filters:    Number of filters in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       Array with negative data converted to 0 and positive data equal 
*       to input.
**************************************************************************/
long double *reLU(long double *data, int filters, int rows, int columns){
    int i;
    long double *ptrData = &data[0];
    for(i = 0; i < filters*rows*columns; ++i){
        if(*ptrData < 0)
            *ptrData = 0;
        ++ptrData;
    } // for i:data
    return data;
} // end reLU

/**************************************************************************
*   Function:   softmax()
*   Purpose:  Softmax Layer ReLU
*   Arguments:
*       output_fully_connected: Array with the output data of 
*                               fully connected.
*   Return:
*       Array with output data of activation layer
**************************************************************************/
long double *softmax(long double* input, int numClasses){
    int i;
    long double sum_array = 0,*ptrInput = &input[0];
    for(i = 0;i<numClasses; ++i){
        *ptrInput = exp(*ptrInput);
        sum_array += *ptrInput;
        ++ptrInput;
    } // end if
    ptrInput=&input[0];
    for(i = 0;i<numClasses; ++i){
        *ptrInput /= sum_array;
        ++ptrInput;
    } // i: NUM_CLASS
    return input;
} // end softmax