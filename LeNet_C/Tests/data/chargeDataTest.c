#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void printData(long double*,int,int,int,int);
int chargeDataTest(long double**,int,int,int);

int main(){
    long double *dataTest;
    int positionLayer;
    chargeDataTest(&dataTest,20,28,28);
    printData(dataTest,20,1,28,28);
}

/**************************************************************************
*   Function:   printData()
*   Purpose:    This is the function for test this layer.
*   Arguments:
*       data:       Array with the output data of this layer.
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       void
**************************************************************************/
void printData(long double* data, int filters, int channels, int rows, int columns){
    int filterPostion,channelPosition,columnPosition,rowPosition,dataPosition=0;
    for(filterPostion = 0;filterPostion<filters;++filterPostion){
        for(channelPosition = 0;channelPosition<channels;++channelPosition){
            printf(" {");
            for(columnPosition = 0;columnPosition<rows;++columnPosition){
                printf(" [");
                for(rowPosition = 0;rowPosition<columns;++rowPosition){
                    printf("%.19Lf ",data[dataPosition]);
                    dataPosition++;
                } // end for rowPosition: newColumns
                printf("]\n");
            } // end for columnPosition: newRows
            printf("}\n");
        } // end for channelPosition: channels
        printf("\n");
        printf("\n");
    } // end for filterPostion: filters
} // end printData

/**************************************************************************
*   Function:   chargeWeights()
*   Purpose:    This is the function for charge weights and parameters 
*               for layers in the CNN model.
*   Arguments:
*       weightsP:       Array passed by reference to store the kernels 
*                       weights of the layers in the CCN model.
*       biasP:          Array passed by reference to store the bias 
*                       weights of the layers in the CCN model.
*       layersInfoP:    Array with the information of layers on the 
*                       CNN model.
*   Return:
*       Number of layers with weights in the CNN.
**************************************************************************/
int chargeDataTest(long double**dataTestP, int filters, int rows, int columns){
    int i;
    long double *dataTest=(long double*)calloc(filters*rows*columns,sizeof(long double)),*ptrDataTest=&dataTest[0];
    char *filename="outputConv2D_1.dat",*contents=NULL,*token=NULL,*ptrTrash;
    FILE *fp=fopen(filename,"r");
    if (!fp){
        printf("El archivo %s no se puede abrir o no existe\n",filename);
        exit(EXIT_FAILURE);
    }   // end if error open file filename
    size_t len=0;
    while (getline(&contents, &len, fp) != -1){
        token=strtok(contents,",");
        for(i=0;i<columns;++i){
            *ptrDataTest=strtold(token,&ptrTrash);
            ++ptrDataTest;
            token=strtok(NULL,",");
        }
    } // end file weights.cfg
    fclose(fp);// close the file
    *dataTestP=dataTest;
    return 1;
} // end chargeWeights