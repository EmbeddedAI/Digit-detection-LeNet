#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct LayerInfo LayerInfo;
struct LayerInfo{
    char *name;
    char flagBias;
    int sizeBias;
    int sizeKernel;
};

int chargeWeights(long double***,long double***,LayerInfo**);

int main(){
    long double **weights,**bias;
    int positionLayer,i,numLayers;
    LayerInfo *layersInfo;
    numLayers=chargeWeights(&weights,&bias,&layersInfo);
    for(positionLayer=0;positionLayer<numLayers;++positionLayer){
        for(i=0;i<layersInfo[positionLayer].sizeBias;++i){
            printf("Layer: %d Pos: %d %.19LF\n",positionLayer,i,bias[positionLayer][i]);
        } // end for i: layersInfo[positionLayer].sizeBias
    } // end for positionLayer: numLayers
    for(positionLayer=0;positionLayer<numLayers;++positionLayer){
        for(i=0;i<layersInfo[positionLayer].sizeKernel;++i){
            printf("Layer: %d Pos: %d %.19LF\n",positionLayer,i,weights[positionLayer][i]);
        } // end for i: layersInfo[positionLayer].sizeKernel
    } // end for positionLayer: numLayers
    return 0;
}

/**************************************************************************
*   Function:   chargeWeights()
*   Purpose:    This is the function for charge weights and parameters 
*               for layers in the CNN model.
*   Arguments:
*       weightsP:       Array passed by reference to store the kernels 
*                       weights of the layers in the CCN model.
*       biasP:          Array passed by reference to store the bias 
*                       weights of the layers in the CCN model.
*       layersInfoP:    Array with the information of layers on the 
*                       CNN model.
*   Return:
*       Number of layers with weights in the CNN.
**************************************************************************/
int chargeWeights(long double***weightsP, long double ***biasP, LayerInfo **layersInfoP){
    long double **weights,**bias,*ptrWeights,*ptrBias;
    char *filename="weights.cfg",*filenameBias="weightsBias.cfg",*filenameKernel="weightsKernel.cfg",flagBias,flagBiasGeneral,*contents=NULL,*token=NULL,*ptrTrash;
    int i,numLayers,positionLayer=0;
    LayerInfo *layersInfo;
    FILE *fp=fopen(filename,"r");
    if (!fp){
        printf("El archivo %s no se puede abrir o no existe\n",filename);
        exit(EXIT_FAILURE);
    }   // end if error open file filename
    size_t len=0;
    getline(&contents, &len, fp);
    token=strtok(contents,",");
    numLayers=atoi(token);
    token=strtok(NULL,",");
    flagBiasGeneral=atoi(token);
    weights=(long double**)calloc(numLayers,sizeof(long double*));
    layersInfo=(LayerInfo*)calloc(numLayers,sizeof(LayerInfo));
    if(flagBiasGeneral)
        bias=(long double**)calloc(numLayers,sizeof(long double*));
    printf("NumLayers: %d, FlagBias: %d\n",numLayers,flagBiasGeneral);
    while (getline(&contents, &len, fp) != -1){
        token=strtok(contents,",");
        layersInfo[positionLayer].name = (char*)calloc(strlen(token),sizeof(char));
        strcpy(layersInfo[positionLayer].name,token);
        token=strtok(NULL,",");
        flagBias=atoi(token);
        layersInfo[positionLayer].flagBias=flagBias;
        if(flagBias){
            token=strtok(NULL,",");
            layersInfo[positionLayer].sizeBias=atoi(token);
            bias[positionLayer]=(long double*)calloc(layersInfo[positionLayer].sizeBias,sizeof(long double));
            printf("SizeArray Bias: %d\n",layersInfo[positionLayer].sizeBias);
        }else{
            layersInfo[positionLayer].sizeBias=0;
        }
        token=strtok(NULL,",");
        layersInfo[positionLayer].sizeKernel=atoi(token);
        weights[positionLayer]=(long double*)calloc(layersInfo[positionLayer].sizeKernel,sizeof(long double));
        printf("SizeArray Kernel: %d\n",layersInfo[positionLayer].sizeKernel);
        ++positionLayer;
    } // end file weights.cfg
    positionLayer=0;
    fclose(fp);// close the file
    if(flagBiasGeneral){
        fp=fopen(filenameBias,"r");
        if (!fp){
            printf("El archivo %s no se puede abrir o no existe\n",filenameBias);
            exit(EXIT_FAILURE);
        } // end if error open file filenameBias
        for(positionLayer=0;positionLayer<numLayers;++positionLayer){
            getline(&contents, &len, fp);
            token=strtok(contents,",");
            ptrBias = bias[positionLayer];
            for(i=0;i<layersInfo[positionLayer].sizeBias;++i){
                *ptrBias=strtold(token,&ptrTrash);
                ++ptrBias;
                token=strtok(NULL,",");
            } // end for i: layersInfo[positionLayer].sizeBias
        } // end for positionLayer: numLayers
        fclose(fp);// close the file of bias weights
    } // end if flagBiasGeneral
    fp=fopen(filenameKernel,"r");
    if (!fp){
        printf("El archivo %s no se puede abrir o no existe\n",filenameKernel);
        exit(EXIT_FAILURE);
    } // end if error open file filenameKernel
    for(positionLayer=0;positionLayer<numLayers;++positionLayer){
        getline(&contents, &len, fp);
        //printf("%s\n",contents);
        token=strtok(contents,",");
        ptrWeights = weights[positionLayer];
        for(i=0;i<layersInfo[positionLayer].sizeKernel;++i){
            *ptrWeights=strtold(token,&ptrTrash);
            ++ptrWeights;
            token=strtok(NULL,",");
        } // end for i: layersInfo[positionLayer].sizeKernel
    } // end for positionLayer: numLayers
    fclose(fp);// close the file of kernel weights
    *weightsP=weights;
    *biasP=bias;
    *layersInfoP=layersInfo;
    return numLayers;
} // end chargeWeights