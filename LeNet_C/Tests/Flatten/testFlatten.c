#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

/**************************************************************************
*                            Constans Defintion                           *
**************************************************************************/
#define NUM_CLASS 10
/**************************************************************************
*                           Protoypes Functions                           *
**************************************************************************/
int chargeDataTest(char*,long double**,int,int,int);
void printData(long double*,int,int,int,int);
char test(long double*,long double*,int,int,int,int);
/**************************************************************************
*                        Layers Protoypes Functions                       *              
**************************************************************************/
long double *flatten(long double*,int,int,int);

int main(){
    char *filename="data/outputMaxPooling2D_2.dat",*filenameTest="data/outputFlatten.dat";
    int channels=50,rows=7,columns=7,inputNeurons=channels*rows*columns,outputNeurons=500;
    long double *dataInput,*dataTest,*data;
    chargeDataTest(filename,&dataInput,channels,rows,columns);
    chargeDataTest(filenameTest,&dataTest,1,1,inputNeurons);
    data=flatten(dataInput,channels,rows,columns);
    printData(data,1,1,1,inputNeurons);
    printf("Input Neurons: %d\n",inputNeurons);
    printf("Result: ");
    if(test(data,dataTest,1,1,1,inputNeurons))
        printf("Error\n");
    else
        printf("OK\n");
    return 0;
} // end main

/**************************************************************************
*   Function:   chargeWeights()
*   Purpose:    This is the function for charge weights and parameters 
*               for layers in the CNN model.
*   Arguments:
*       weightsP:       Array passed by reference to store the kernels 
*                       weights of the layers in the CCN model.
*       biasP:          Array passed by reference to store the bias 
*                       weights of the layers in the CCN model.
*       layersInfoP:    Array with the information of layers on the 
*                       CNN model.
*   Return:
*       Number of layers with weights in the CNN.
**************************************************************************/
int chargeDataTest(char *filename, long double**dataTestP, int filters, int rows, int columns){
    int i;
    long double *dataTest=(long double*)calloc(filters*rows*columns,sizeof(long double)),*ptrDataTest=&dataTest[0];
    char *contents=NULL,*token=NULL,*ptrTrash;
    FILE *fp=fopen(filename,"r");
    if (!fp){
        printf("El archivo %s no se puede abrir o no existe\n",filename);
        exit(EXIT_FAILURE);
    }   // end if error open file filename
    size_t len=0;
    while (getline(&contents, &len, fp) != -1){
        token=strtok(contents,",");
        for(i=0;i<columns;++i){
            *ptrDataTest=strtold(token,&ptrTrash);
            ++ptrDataTest;
            token=strtok(NULL,",");
        }
    } // end file weights.cfg
    fclose(fp);// close the file
    *dataTestP=dataTest;
    return 1;
} // end chargeDataTest

/**************************************************************************
*   Function:   printData()
*   Purpose:    This is the function for test this layer.
*   Arguments:
*       data:       Array with the output data of this layer.
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       void
**************************************************************************/
void printData(long double* data, int filters, int channels, int rows, int columns){
    int filterPostion,channelPosition,columnPosition,rowPosition,dataPosition=0;
    for(filterPostion = 0;filterPostion<filters;++filterPostion){
        for(channelPosition = 0;channelPosition<channels;++channelPosition){
            for(columnPosition = 0;columnPosition<rows;++columnPosition){
                printf(" ");
                for(rowPosition = 0;rowPosition<columns;++rowPosition){
                    printf("%.18Lf ",data[dataPosition]);
                    dataPosition++;
                } // end for rowPosition: newColumns
                printf("\n");
            } // end for columnPosition: newRows
            printf("\n");
        } // end for channelPosition: channels
        printf("\n");
        printf("\n");
    } // end for filterPostion: filters
} // end printData

/**************************************************************************
*   Function:   test()
*   Purpose:    This is the function for test this layer.
*   Arguments:
*       data:       Array with the output data of this layer.
*       dataTest:   Array with the expected output data of this layer
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       Boolean for indicate if the result layer is Error or Correct.
*       1 - Error
*       0 - Correct
**************************************************************************/
char test(long double *data, long double *dataTest, int filters, int channels, int columns, int rows){
    long double comparator = 0.0000000000000000000; // conv2D_1 maxPooling2D_1
    //long double comparator = 0.000004; // conv2D_2 maxPooling2D_2
    for(int i = 0; i<filters*channels*columns*rows;++i){
        if(fabsl(dataTest[i]-data[i])>comparator){
            printf("Error in: dataTest[%d] = %.19LF  data[%d] = %.19LF Rest= %.19LF \n",i,dataTest[i],i,data[i],fabsl(dataTest[i]-data[i]));
            return 1;
        }
    }
    return 0;
} // end test

/**************************************************************************
 *                                 Layers                                 *
 **************************************************************************/
/**************************************************************************
*   Función:   flatten()
*   Proposito:  Performs the transposition that performs the Flatten 
*               layer in Python Keras
*   Argumentos:
*       data:   Array with the input data of this layer.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Retorno:
*       Array with output of this layer
**************************************************************************/
long double *flatten(long double *data, int channels, int rows, int columns){
    int dataPosition,channelPosition=0,columnPosition=0,rowPosition=0;
    long double *dataFlatten=(long double*)calloc(channels*rows*columns,sizeof(long double));
    for(dataPosition=0;dataPosition<channels*rows*columns;++dataPosition){
        dataFlatten[rowPosition*rows*channels+channelPosition*rows+columnPosition]=data[channelPosition*rows*columns+columnPosition*columns+rowPosition];
        ++rowPosition;
        if(rowPosition==columns){
            rowPosition=0;
            ++columnPosition;
            if(columnPosition==rows){
                columnPosition=0;
                ++channelPosition;
            } // end if rows
        } // end if columns
    } // end for dataFlatten
    return dataFlatten;
} // end flatten