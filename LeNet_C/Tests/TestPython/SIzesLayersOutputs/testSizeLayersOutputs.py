import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import tensorflow as tf
from tensorflow import keras
import keras.layers as layers

numChannels, imgRows, imgCols, batch_size = 1, 10, 10, 128
numClasses = 10
inputShape = (batch_size, imgRows, imgCols, numChannels)
activation = 'relu' 
x = tf.random.normal(inputShape)
print("X:", x.shape)
y = layers.Conv2D(filters=20, kernel_size=3, strides=(2, 2), padding="same", activation=activation, input_shape=inputShape[1:])(x)
print("y:",y.shape)
# z = layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2))(y)
# print("z:",z.shape)
# a = layers.Conv2D(filters=50, kernel_size=5, padding="same", activation=activation)(z)
# print("a:",a.shape)
# b = layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2))(a)
# print("b:",b.shape)
# c = layers.Flatten()(b)
# print("c:",c.shape)