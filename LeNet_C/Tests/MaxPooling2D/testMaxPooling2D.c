#include <stdlib.h>
#include <stdio.h>

/**************************************************************************
*                           Protoypes Functions                           *
**************************************************************************/
void printData(long double*,int,int,int,int);
char test(long double*,long double*,int,int,int,int);
/**************************************************************************
*                        Layers Protoypes Functions                       *              
**************************************************************************/
long double *maxPooling2D(long double*,int,int,int,int,int);

int main(){ 
    int filters=2,rows=10,columns=10,poolSize=2,stride=2,newColumns=(columns-poolSize)/stride+1,newRows=(rows-poolSize)/stride+1;
    long double dataInput[200] = {85.0,135.0,150.0,165.0,180.0,195.0,210.0,225.0,240.0,165.0,780.0,1173.0,1179.0,1185.0,1191.0,1197.0,1203.0,1209.0,1215.0,812.0,820.0,1233.0,1239.0,1245.0,1251.0,1257.0,1263.0,1269.0,1275.0,852.0,860.0,1293.0,1299.0,1305.0,1311.0,1317.0,1323.0,1329.0,1335.0,892.0,900.0,1353.0,1359.0,1365.0,1371.0,1377.0,1383.0,1389.0,1395.0,932.0,940.0,1413.0,1419.0,1425.0,1431.0,1437.0,1443.0,1449.0,1455.0,972.0,980.0,1473.0,1479.0,1485.0,1491.0,1497.0,1503.0,1509.0,1515.0,1012.0,1020.0,1533.0,1539.0,1545.0,1551.0,1557.0,1563.0,1569.0,1575.0,1052.0,1060.0,1593.0,1599.0,1605.0,1611.0,1617.0,1623.0,1629.0,1635.0,1092.0,296.0,441.0,435.0,429.0,423.0,417.0,411.0,405.0,399.0,264.0,343.0,617.0,619.0,621.0,623.0,625.0,627.0,629.0,631.0,553.0,689.0,930.0,933.0,936.0,939.0,942.0,945.0,948.0,951.0,565.0,709.0,960.0,963.0,966.0,969.0,972.0,975.0,978.0,981.0,585.0,729.0,990.0,993.0,996.0,999.0,1002.0,1005.0,1008.0,1011.0,605.0,749.0,1020.0,1023.0,1026.0,1029.0,1032.0,1035.0,1038.0,1041.0,625.0,769.0,1050.0,1053.0,1056.0,1059.0,1062.0,1065.0,1068.0,1071.0,645.0,789.0,1080.0,1083.0,1086.0,1089.0,1092.0,1095.0,1098.0,1101.0,665.0,809.0,1110.0,1113.0,1116.0,1119.0,1122.0,1125.0,1128.0,1131.0,685.0,829.0,1140.0,1143.0,1146.0,1149.0,1152.0,1155.0,1158.0,1161.0,705.0,445.0,763.0,765.0,767.0,769.0,771.0,773.0,775.0,777.0,750.0};
    //1,1 long double dataTest[200] = {85.0,135.0,150.0,165.0,180.0,195.0,210.0,225.0,240.0,165.0,780.0,1173.0,1179.0,1185.0,1191.0,1197.0,1203.0,1209.0,1215.0,812.0,820.0,1233.0,1239.0,1245.0,1251.0,1257.0,1263.0,1269.0,1275.0,852.0,860.0,1293.0,1299.0,1305.0,1311.0,1317.0,1323.0,1329.0,1335.0,892.0,900.0,1353.0,1359.0,1365.0,1371.0,1377.0,1383.0,1389.0,1395.0,932.0,940.0,1413.0,1419.0,1425.0,1431.0,1437.0,1443.0,1449.0,1455.0,972.0,980.0,1473.0,1479.0,1485.0,1491.0,1497.0,1503.0,1509.0,1515.0,1012.0,1020.0,1533.0,1539.0,1545.0,1551.0,1557.0,1563.0,1569.0,1575.0,1052.0,1060.0,1593.0,1599.0,1605.0,1611.0,1617.0,1623.0,1629.0,1635.0,1092.0,296.0,441.0,435.0,429.0,423.0,417.0,411.0,405.0,399.0,264.0,343.0,617.0,619.0,621.0,623.0,625.0,627.0,629.0,631.0,553.0,689.0,930.0,933.0,936.0,939.0,942.0,945.0,948.0,951.0,565.0,709.0,960.0,963.0,966.0,969.0,972.0,975.0,978.0,981.0,585.0,729.0,990.0,993.0,996.0,999.0,1002.0,1005.0,1008.0,1011.0,605.0,749.0,1020.0,1023.0,1026.0,1029.0,1032.0,1035.0,1038.0,1041.0,625.0,769.0,1050.0,1053.0,1056.0,1059.0,1062.0,1065.0,1068.0,1071.0,645.0,789.0,1080.0,1083.0,1086.0,1089.0,1092.0,1095.0,1098.0,1101.0,665.0,809.0,1110.0,1113.0,1116.0,1119.0,1122.0,1125.0,1128.0,1131.0,685.0,829.0,1140.0,1143.0,1146.0,1149.0,1152.0,1155.0,1158.0,1161.0,705.0,445.0,763.0,765.0,767.0,769.0,771.0,773.0,775.0,777.0,750.0};
    //4,2 long double dataTest[32] = {1305.0,1317.0,1329.0,1335.0,1425.0,1437.0,1449.0,1455.0,1545.0,1557.0,1569.0,1575.0,1605.0,1617.0,1629.0,1635.0,996.0,1002.0,1008.0,1011.0,1056.0,1062.0,1068.0,1071.0,1116.0,1122.0,1128.0,1131.0,1146.0,1152.0,1158.0,1161.0};
    //2,2 
    long double dataTest[50] =  {1173.0,1185.0,1197.0,1209.0,1215.0,1293.0,1305.0,1317.0,1329.0,1335.0,1413.0,1425.0,1437.0,1449.0,1455.0,1533.0,1545.0,1557.0,1569.0,1575.0,1593.0,1605.0,1617.0,1629.0,1635.0,930.0,936.0,942.0,948.0,951.0,990.0,996.0,1002.0,1008.0,1011.0,1050.0,1056.0,1062.0,1068.0,1071.0,1110.0,1116.0,1122.0,1128.0,1131.0,1140.0,1146.0,1152.0,1158.0,1161.0};
    //3,3 long double dataTest[18] = {1239.0,1257.0,1275.0,1419.0,1437.0,1455.0,1599.0,1617.0,1635.0,963.0,972.0,981.0,1053.0,1062.0,1071.0,1143.0,1152.0,1161.0};
    printf("Data\n");
    printData(dataInput,filters,1,rows,columns);
    long double *data = maxPooling2D(dataInput,filters,rows,columns,poolSize,stride);
    printf("Result: ");
    if(test(data,dataTest,filters,1,newColumns,newRows))
        printf("Error\n");
    else
        printf("OK\n");
    printData(data,filters,1,newRows,newColumns);
    return 0;
} // end main

/**************************************************************************
*   Function:   printData()
*   Purpose:    This is the function for test this layer.
*   Arguments:
*       data:       Array with the output data of this layer.
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       void
**************************************************************************/
void printData(long double* data, int filters, int channels, int rows, int columns){
    int filterPostion,channelPosition,columnPosition,rowPosition,dataPosition=0;
    for(filterPostion = 0;filterPostion<filters;++filterPostion){
        for(channelPosition = 0;channelPosition<channels;++channelPosition){
            for(columnPosition = 0;columnPosition<rows;++columnPosition){
                printf(" ");
                for(rowPosition = 0;rowPosition<columns;++rowPosition){
                    printf("%.1Lf ",data[dataPosition]);
                    dataPosition++;
                } // end for rowPosition: newColumns
                printf("\n");
            } // end for columnPosition: newRows
            printf("\n");
        } // end for channelPosition: channels
        printf("\n");
        printf("\n");
    } // end for filterPostion: filters
} // end printData

/**************************************************************************
*   Function:   test()
*   Purpose:    This is the function for test this layer.
*   Arguments:
*       data:       Array with the output data of this layer.
*       dataTest:   Array with the expected output data of this layer
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       Boolean for indicate if the result layer is Error or Correct.
*       1 - Error
*       0 - Correct
**************************************************************************/
char test(long double *data, long double *dataTest, int filters, int channels, int columns, int rows){
    for(int i = 0; i<filters*channels*columns*rows;++i)
        if(dataTest[i] != data[i])
            return 1;
    return 0;
} // end test

/**************************************************************************
 *                                 Layers                                 *
 **************************************************************************/
/**************************************************************************
*   Function:   maxPooling2D()
*   Purpose:    The maximum of the positions covered by the pool is taken,
*               which is of size poolSize x poolSize, this pool runs
*               through the entire matrix jumping in steps defined in the
*               stride, until it obtains a new matrix with all the maximums,
*               this being smaller than the original by equation 1 and 2
*               that we see in the image.
*   Arguments:
*       data:       Array with the input data of this Conv2D layer.
*       filters:    Number of filters in the data array
*       rows:       Number of rows of each channel in the data array.
*       cols:       Number of cols of each channel in the data array.
*       poolSize:   Pool size of this Conv2D layer.
*   Return:
*       Array with output of this layer MaxPooling2D.
**************************************************************************/
long double *maxPooling2D(long double *data, int filters, int rows, int columns, int poolSize, int stride){
    int timesKernel,poolRowsPosition=0,poolColumnsPosition=0,rowPosition=0,columnPosition=0,newColumns=(columns-poolSize)/stride+1,newRows=(rows-poolSize)/stride+1,poolTop = poolSize*poolSize;
    long double *ptrData = &data[0], *ptrColumnsBeginData = &data[0],*ptrBeginData = &data[0], maxComparator = -10000;
    long double *dataNew = (long double*)calloc(filters*newColumns*newRows,sizeof(long double)), *ptrDataNew = &dataNew[0];
    int dataPosition=0,beginDataPosition=0,columnsBeginDataPosition=0;
    for(timesKernel=0;timesKernel<=filters*newColumns*newRows*poolTop;++timesKernel){
        if(rowPosition+poolSize-1 > columns-1){
            //printf("%d == %d",rowPosition+poolSize-1,columns);
            rowPosition = 0;
            poolRowsPosition = 0;
            poolColumnsPosition = 0;
            columnPosition +=stride;
            if(columnPosition+poolSize-1 > rows+-1){
                ptrData = ptrColumnsBeginData+(columns-columnPosition+stride)*columns;
                columnPosition = 0;
                ptrColumnsBeginData = ptrData;
                ptrBeginData = ptrData;
                // test //
                dataPosition = columnsBeginDataPosition+poolSize*columns;
                columnsBeginDataPosition = dataPosition;
                beginDataPosition = dataPosition;
                // test //
            }else{
                ptrData = ptrColumnsBeginData + stride*columns;
                ptrColumnsBeginData = ptrData;
                ptrBeginData = ptrData;
                // test //
                dataPosition = columnsBeginDataPosition + stride*columns;
                columnsBeginDataPosition = dataPosition;
                beginDataPosition = dataPosition;
                // test //
            }// end if columnPosition: rows
        }// end if rowPosition: column
        if(*ptrData > maxComparator)
            maxComparator = *ptrData;
        //printf("Datapostion: %d | Data: %f\n",dataPosition,*ptrData);
        ++poolRowsPosition;
        ++ptrData;
        // test //
        ++dataPosition;
        // test //
        if(poolRowsPosition == poolSize){
            poolRowsPosition = 0;
            ptrData += (columns-poolSize);
            // test //
            dataPosition += (columns-poolSize);
            // test //
            ++poolColumnsPosition;
            if(poolColumnsPosition==poolSize){
                poolColumnsPosition = 0;
                rowPosition += stride;
                *ptrDataNew = maxComparator;
                ++ptrDataNew;
                maxComparator = -10000;
                ptrData = ptrBeginData+stride;
                ptrBeginData = ptrData;
                // test //
                dataPosition = beginDataPosition+stride;
                beginDataPosition = dataPosition;
                // test //
            } // end if poolColumnsPosition: poolSize
        } // end if poolRowsPosition: poolSize
    } // end for timesKernel: filters*channels*newColumns*newRows*poolTop
    return dataNew;
} // end maxPooling2D