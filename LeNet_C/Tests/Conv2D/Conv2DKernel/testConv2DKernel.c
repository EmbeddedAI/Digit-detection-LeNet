#include <stdlib.h>
#include <stdio.h>
/* Measure execution time */
#include <time.h>

/**************************************************************************
*                           Prototypes Functions                           *
**************************************************************************/
void printData(float*,int,int,int,int);
char test(float*,float*,int,int,int,int);
/**************************************************************************
*                        Layers Prototypes Functions                       *              
**************************************************************************/
float *sumChannels(float*,float*,float,int,int,int,int);
float *conv2DKernel(float*,float*,float*,int,int,int,int,int,int);

int main(){
    int filters=1,channels=1,rows=5,columns=5,kernelSize=5,stride=1,newRows=(rows-kernelSize)/stride+1,newColumns=(columns-kernelSize)/stride+1;
    float dataInput[25] = {230,227,224,221,218,200,197,194,191,188,170,167,164,161,158,140,137,134,131,128,110,107,104,101,98};
    float bias[2] = {0.0};
    float kernel[25] = {1,1,1,1.3,1,2.1,0,0,0.2,1.5,0,0,0,0.5,1,2,0,1.2,2,1.5,2.1,0.6,0,0.2,0.6};
    float dataTest[1] = {3434};
    printf("Data\n");
    printData(dataInput,1,channels,rows,columns);
    printf("Kernel\n");
    printData(kernel,filters,channels,kernelSize,kernelSize);
    float *data;
    long long nanoseconds,average=0;
    // Start measuring time
    struct timespec begin, end; 
    int count=0;
    int max=5000;
    while(count<max){
        clock_gettime(CLOCK_REALTIME, &begin);
        data = conv2DKernel(dataInput,bias,kernel,filters,channels,rows,columns,kernelSize,stride);
        clock_gettime(CLOCK_REALTIME, &end);
        nanoseconds = end.tv_nsec - begin.tv_nsec;
        average+=nanoseconds;
        count++;
    }
    average/=max;
    printf("Time Average(ns): %lld\n",average);
    printf("Result: ");
    if(test(data,dataTest,filters,1,newRows,newColumns))
        printf("Error\n");
    else
        printf("OK\n");
    printData(data,filters,1,newRows,newColumns);
    return 0;
} // end main

/**************************************************************************
*   Function:   printData()
*   Purpose:    This is the function for test this layer.
*   Arguments:
*       data:       Array with the output data of this layer.
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       void
**************************************************************************/
void printData(float* data, int filters, int channels, int rows, int columns){
    int filterPostion,channelPosition,columnPosition,rowPosition,dataPosition=0;
    for(filterPostion = 0;filterPostion<filters;++filterPostion){
        for(channelPosition = 0;channelPosition<channels;++channelPosition){
            for(columnPosition = 0;columnPosition<rows;++columnPosition){
                printf(" ");
                for(rowPosition = 0;rowPosition<columns;++rowPosition){
                    printf("%.1f ",data[dataPosition]);
                    dataPosition++;
                } // end for rowPosition: newColumns
                printf("\n");
            } // end for columnPosition: newRows
            printf("\n");
        } // end for channelPosition: channels
        printf("\n");
        printf("\n");
    } // end for filterPostion: filters
} // end printData

/**************************************************************************
*   Function:   test()
*   Purpose:    This is the function for test this layer.
*   Arguments:
*       data:       Array with the output data of this layer.
*       dataTest:   Array with the expected output data of this layer
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       Boolean for indicate if the result layer is Error or Correct.
*       1 - Error
*       0 - Correct
**************************************************************************/
char test(float *data, float *dataTest, int filters, int channels, int columns, int rows){
    for(int i = 0; i<filters*channels*columns*rows;++i)
        if(dataTest[i] != data[i])
            return 1;
    return 0;
} // end test

/**************************************************************************
 *                                 Layers                                 *
 **************************************************************************/
/**************************************************************************
*   Function:   sumChannels()
*   Purpose:    Sum by each position of the three channel for obtain one
*               array with the result of convolution of one filter.
*   Arguments:
*       data:           Array with the ouput data of this Conv2D layer.
*       dataFilter:     Array with convolution calculate by each channel.
*       bias:           Bias of this filter.
*       channels:       Number of channels of each filter in the data array.
*       startFilter:    Position of the filter in data array.
*       rows:           Number of rows of each filter in the data array.
*       columns:        Number of cols of each channel in the data array.
*   Return:
*       Array with sum of three channels by each position.
**************************************************************************/
float *sumChannels(float *data, float *dataFilter, float bias, int channels, int startFilter, int rows, int columns){
    float *ptrData=&data[startFilter], **ptrChannels = (float**)calloc(channels,sizeof(float*)), **ptrBeginChannels=&ptrChannels[0],sumAux = 0;
    int channelPosition,dataPosition;
    for(channelPosition=0;channelPosition<channels;++channelPosition){
        *ptrChannels = &dataFilter[channelPosition*rows*columns];
        ++ptrChannels;
    }
    ptrChannels = ptrBeginChannels;
    for(dataPosition=0;dataPosition<rows*columns;++dataPosition){
        for(channelPosition=0;channelPosition<channels;++channelPosition){
            sumAux += **ptrChannels;
            ++*ptrChannels;
            ++ptrChannels;
        }
        ptrChannels = ptrBeginChannels;
        *ptrData = sumAux + bias;
        sumAux = 0;
        ++ptrData;
    }
    return data;
} // end sumChannels

/**************************************************************************
*   Function:   conv2DKernel()
*   Purpose:    This is the second block of Conv2D  is in charge of 
*               performing the convolution with the kernels of the input
*               data.
*   Arguments:
*       data:       Array with the input data of this Conv2D layer.
*       kernel:     Array with the weights of the kernels of this layer.
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       cols:       Number of cols of each channel in the data array.
*       kernelSize: Kernel size of this Conv2D layer.
*   Return:
*       Array with output of this block from the Conv2D.
**************************************************************************/
float *conv2DKernel(float *data, float *bias, float *kernel, int filters, int channels, int rows, int columns, int kernelSize, int stride){
    int timesKernel,kernelRowsPosition=0,kernelColumnsPosition=0,rowPosition=0,columnPosition=0,newCols=(columns-kernelSize)/stride+1,newRows=(rows-kernelSize)/stride+1,kernelTop = kernelSize*kernelSize;
    int channelPosition = 0,filterPosition=0;
    float *ptrData = &data[0], *ptrBias = &bias[0], *ptrKernel = &kernel[0], *ptrColumnsBeginData = &data[0],*ptrBeginData = &data[0], *ptrBeginKernel = &kernel[0],sumProduct=0;
    float *dataNew = (float*)calloc(filters*newCols*newRows,sizeof(float)), *ptrDataNew = &dataNew[0];
    float *dataFilter = (float*)calloc(channels*newCols*newRows,sizeof(float)), *ptrDataFilter = &dataFilter[0];
    for(timesKernel=0;timesKernel<=filters*channels*newCols*newRows*kernelTop;++timesKernel){
        if(rowPosition+kernelSize-1 > columns-1){
            rowPosition = 0;
            kernelRowsPosition = 0;
            kernelColumnsPosition = 0;
            ptrData = ptrColumnsBeginData + stride*columns;
            ptrColumnsBeginData = ptrData;
            ptrBeginData = ptrData;
            columnPosition += stride;
            if(columnPosition+kernelSize-1 > rows-1){
                columnPosition = 0;
                ++channelPosition;
                ptrData = ptrColumnsBeginData + (kernelSize-1)*columns;
                ptrColumnsBeginData = ptrData;
                ptrBeginData = ptrData;
                ptrKernel = ptrBeginKernel + kernelTop;
                ptrBeginKernel = ptrKernel;
                if(channelPosition == channels){
                    channelPosition = 0;
                    if(channels > 1)
                        dataNew = sumChannels(dataNew,dataFilter,*ptrBias,channels,filterPosition*newRows*newCols,newRows,newCols);
                    ++ptrBias;
                    ++filterPosition;
                    ptrData = &data[0];
                    ptrColumnsBeginData = &data[0];
                    ptrBeginData = &data[0];
                    ptrDataFilter = &dataFilter[0];
                }
            }else{
                ptrKernel = ptrBeginKernel;
            } // end if columnPosition: rows
        }// end if rowPosition: columns
        sumProduct += (*ptrData**ptrKernel);//(data[dataPostion]*kernel[kernelPosition])
        ++ptrKernel;
        ++kernelRowsPosition;
        ++ptrData;
        if(kernelRowsPosition == kernelSize){
            kernelRowsPosition = 0;
            ptrData += (columns-kernelSize);
            ++kernelColumnsPosition;
            if(kernelColumnsPosition==kernelSize){
                kernelColumnsPosition = 0;
                rowPosition += stride;
                if(channels == 1){
                    *ptrDataNew = (sumProduct+*ptrBias);
                    ++ptrDataNew;
                }else{
                    *ptrDataFilter = sumProduct;
                    ++ptrDataFilter;
                }
                sumProduct = 0;
                ptrData = ptrBeginData+stride;
                ptrBeginData = ptrData;
                ptrKernel = ptrBeginKernel;
            } // end if kernelColumnsPosition: kernelSize
        } // end if kernelRowsPosition: kernelSize
    } // end for timesKernel: filters*channels*newCols*newRows*kernelTop
    return dataNew;
} // end conv2DKernel