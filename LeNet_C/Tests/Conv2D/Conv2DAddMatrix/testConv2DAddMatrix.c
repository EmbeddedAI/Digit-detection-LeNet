#include <stdlib.h>
#include <stdio.h>

/**************************************************************************
*                          Enumeration Defintion                          *
**************************************************************************/
typedef enum {PaddingTopBottom,Data,PaddingLeftRight}STATE_T;

/**************************************************************************
*                           Prototypes Functions                           *
**************************************************************************/
void printData(float*,int,int,int,int);
char test(float*,float*,int,int,int,int);
/**************************************************************************
*                        Layers Prototypes Functions                       *              
**************************************************************************/
float* conv2DAddMatrix(float*,int,int,int,int);
float* conv2DAddMatrixV2(float*,int,int,int,int);

int main(int argc, char *argv[]){
    int channels=3,rows=10,columns=10,kernelSize=3,newRows = rows+kernelSize-1, newColumns = columns+kernelSize-1;
    float dataInput[300] = {299.0,296.0,293.0,290.0,287.0,284.0,281.0,278.0,275.0,272.0,269.0,266.0,263.0,260.0,257.0,254.0,251.0,248.0,245.0,242.0,239.0,236.0,233.0,230.0,227.0,224.0,221.0,218.0,215.0,212.0,209.0,206.0,203.0,200.0,197.0,194.0,191.0,188.0,185.0,182.0,179.0,176.0,173.0,170.0,167.0,164.0,161.0,158.0,155.0,152.0,149.0,146.0,143.0,140.0,137.0,134.0,131.0,128.0,125.0,122.0,119.0,116.0,113.0,110.0,107.0,104.0,101.0,98.0,95.0,92.0,89.0,86.0,83.0,80.0,77.0,74.0,71.0,68.0,65.0,62.0,59.0,56.0,53.0,50.0,47.0,44.0,41.0,38.0,35.0,32.0,29.0,26.0,23.0,20.0,17.0,14.0,11.0,8.0,5.0,2.0,0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0,19.0,20.0,21.0,22.0,23.0,24.0,25.0,26.0,27.0,28.0,29.0,30.0,31.0,32.0,33.0,34.0,35.0,36.0,37.0,38.0,39.0,40.0,41.0,42.0,43.0,44.0,45.0,46.0,47.0,48.0,49.0,50.0,51.0,52.0,53.0,54.0,55.0,56.0,57.0,58.0,59.0,60.0,61.0,62.0,63.0,64.0,65.0,66.0,67.0,68.0,69.0,70.0,71.0,72.0,73.0,74.0,75.0,76.0,77.0,78.0,79.0,80.0,81.0,82.0,83.0,84.0,85.0,86.0,87.0,88.0,89.0,90.0,91.0,92.0,93.0,94.0,95.0,96.0,97.0,98.0,99.0,0.0,4.0,8.0,12.0,16.0,20.0,24.0,28.0,32.0,36.0,40.0,44.0,48.0,52.0,56.0,60.0,64.0,68.0,72.0,76.0,80.0,84.0,88.0,92.0,96.0,100.0,104.0,108.0,112.0,116.0,120.0,124.0,128.0,132.0,136.0,140.0,144.0,148.0,152.0,156.0,160.0,164.0,168.0,172.0,176.0,180.0,184.0,188.0,192.0,196.0,200.0,204.0,208.0,212.0,216.0,220.0,224.0,228.0,232.0,236.0,240.0,244.0,248.0,252.0,256.0,260.0,264.0,268.0,272.0,276.0,280.0,284.0,288.0,292.0,296.0,300.0,304.0,308.0,312.0,316.0,320.0,324.0,328.0,332.0,336.0,340.0,344.0,348.0,352.0,356.0,360.0,364.0,368.0,372.0,376.0,380.0,384.0,388.0,392.0,396.0};
    float dataTest[432] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,299.0,296.0,293.0,290.0,287.0,284.0,281.0,278.0,275.0,272.0,0.0,0.0,269.0,266.0,263.0,260.0,257.0,254.0,251.0,248.0,245.0,242.0,0.0,0.0,239.0,236.0,233.0,230.0,227.0,224.0,221.0,218.0,215.0,212.0,0.0,0.0,209.0,206.0,203.0,200.0,197.0,194.0,191.0,188.0,185.0,182.0,0.0,0.0,179.0,176.0,173.0,170.0,167.0,164.0,161.0,158.0,155.0,152.0,0.0,0.0,149.0,146.0,143.0,140.0,137.0,134.0,131.0,128.0,125.0,122.0,0.0,0.0,119.0,116.0,113.0,110.0,107.0,104.0,101.0,98.0,95.0,92.0,0.0,0.0,89.0,86.0,83.0,80.0,77.0,74.0,71.0,68.0,65.0,62.0,0.0,0.0,59.0,56.0,53.0,50.0,47.0,44.0,41.0,38.0,35.0,32.0,0.0,0.0,29.0,26.0,23.0,20.0,17.0,14.0,11.0,8.0,5.0,2.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,0.0,0.0,10.0,11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0,19.0,0.0,0.0,20.0,21.0,22.0,23.0,24.0,25.0,26.0,27.0,28.0,29.0,0.0,0.0,30.0,31.0,32.0,33.0,34.0,35.0,36.0,37.0,38.0,39.0,0.0,0.0,40.0,41.0,42.0,43.0,44.0,45.0,46.0,47.0,48.0,49.0,0.0,0.0,50.0,51.0,52.0,53.0,54.0,55.0,56.0,57.0,58.0,59.0,0.0,0.0,60.0,61.0,62.0,63.0,64.0,65.0,66.0,67.0,68.0,69.0,0.0,0.0,70.0,71.0,72.0,73.0,74.0,75.0,76.0,77.0,78.0,79.0,0.0,0.0,80.0,81.0,82.0,83.0,84.0,85.0,86.0,87.0,88.0,89.0,0.0,0.0,90.0,91.0,92.0,93.0,94.0,95.0,96.0,97.0,98.0,99.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,4.0,8.0,12.0,16.0,20.0,24.0,28.0,32.0,36.0,0.0,0.0,40.0,44.0,48.0,52.0,56.0,60.0,64.0,68.0,72.0,76.0,0.0,0.0,80.0,84.0,88.0,92.0,96.0,100.0,104.0,108.0,112.0,116.0,0.0,0.0,120.0,124.0,128.0,132.0,136.0,140.0,144.0,148.0,152.0,156.0,0.0,0.0,160.0,164.0,168.0,172.0,176.0,180.0,184.0,188.0,192.0,196.0,0.0,0.0,200.0,204.0,208.0,212.0,216.0,220.0,224.0,228.0,232.0,236.0,0.0,0.0,240.0,244.0,248.0,252.0,256.0,260.0,264.0,268.0,272.0,276.0,0.0,0.0,280.0,284.0,288.0,292.0,296.0,300.0,304.0,308.0,312.0,316.0,0.0,0.0,320.0,324.0,328.0,332.0,336.0,340.0,344.0,348.0,352.0,356.0,0.0,0.0,360.0,364.0,368.0,372.0,376.0,380.0,384.0,388.0,392.0,396.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
    printf("Data\n");
    printData(dataInput,1,channels,rows,columns);
    float *data = conv2DAddMatrixV2(dataInput,channels,rows,columns,kernelSize);
    printf("Result: \n");
    if(test(data,dataTest,1,channels,newRows,newColumns))
        printf("Error\n");
    else
        printf("OK\n");
    printData(data,1,channels,newRows,newColumns);
    return 0;
} // end main

/**************************************************************************
*   Function:   printData()
*   Purpose:    This is the function for test this layer.
*   Arguments:
*       data:       Array with the output data of this layer.
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       void
**************************************************************************/
void printData(float* data, int filters, int channels, int rows, int columns){
    int filterPostion,channelPosition,columnPosition,rowPosition,dataPosition=0;
    for(filterPostion = 0;filterPostion<filters;++filterPostion){
        for(channelPosition = 0;channelPosition<channels;++channelPosition){
            for(columnPosition = 0;columnPosition<rows;++columnPosition){
                printf(" ");
                for(rowPosition = 0;rowPosition<columns;++rowPosition){
                    printf("%.1f ",data[dataPosition]);
                    dataPosition++;
                } // end for rowPosition: newColumns
                printf("\n");
            } // end for columnPosition: newRows
            printf("\n");
        } // end for channelPosition: channels
        printf("\n");
        printf("\n");
    } // end for filterPostion: filters
} // end printData

/**************************************************************************
*   Function:   test()
*   Purpose:    This is the function for test this layer.
*   Arguments:
*       data:       Array with the output data of this layer.
*       dataTest:   Array with the expected output data of this layer
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       Boolean for indicate if the result layer is Error or Correct.
*       1 - Error
*       0 - Correct
**************************************************************************/
char test(float *data, float *dataTest, int filters, int channels, int columns, int rows){
    for(int i = 0; i<filters*columns*rows;++i)
        if(dataTest[i] != data[i])
            return 1;
    return 0;
} // end test

/**************************************************************************
 *                                 Layers                                 *
 **************************************************************************/
/**************************************************************************
*   Function:   conv2DAddMatrix()
*   Purpose:    This is the first block of Conv2D  is in charge of 
*               handling the padding in the convolution, what it does is 
*               add columns and rows around the matrix according to the 
*               following equations.
*   Arguments:
*       data:       Array with the input data of this Conv2D layer.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       cols:       Number of cols of each channel in the data array.
*       kernelSize: Kernel size of this Conv2D layer.
*   Return:
*       Array with output of this block from the Conv2D.
**************************************************************************/
float* conv2DAddMatrix(float *data, int channels, int rows, int columns, int kernelSize){
    int maxPaddingTopBottom, padding = kernelSize-1, padding2 = padding/2, newColumns = columns+padding,newRows = rows+padding,dataNewPosition,dataPosition=0,colPosition=0,rowPosition=0,filterPosition=0;
    char flagZero, flagBegin=1;
    float *dataNew= (float*)calloc(channels*newRows*newColumns,sizeof(float)), *ptrDataNew = &dataNew[0], *ptrData = &data[0];
    STATE_T state = PaddingTopBottom; 
    for(dataNewPosition=0;dataNewPosition<channels*newRows*newColumns;++dataNewPosition){
        switch (state){
            case PaddingTopBottom:
                flagZero = 0;
                if(flagBegin)
                    maxPaddingTopBottom = padding2*(newColumns+1)-1;
                else
                    maxPaddingTopBottom = 2*(padding2*(newColumns+1))-1;
                if(rowPosition==maxPaddingTopBottom){
                    rowPosition = 0;
                    flagBegin=0;
                    state = Data;
                }else{
                    rowPosition++;
                }
                break;
            
            case Data:
                flagZero = 1;
                if(rowPosition==columns-1){
                    rowPosition = 0;
                    colPosition++;
                    if(colPosition==rows){
                        colPosition=0;
                        state = PaddingTopBottom;
                    }else{
                        state = PaddingLeftRight;
                    }
                }else{
                    rowPosition++;
                }
                break;

            case PaddingLeftRight:
                flagZero = 0;
                if(rowPosition==padding-1){
                    rowPosition = 0;
                    state =  Data;
                }else{
                    rowPosition++;
                }
                break;
        } // end switch state
        if(flagZero){
            *ptrDataNew = *ptrData;//dataNew[dataNewPosition] = data[dataPosition];
            ++ptrData;//dataPosition++;
        }
        ++ptrDataNew;
    } // end for dataNewPosition: data_new
    return dataNew;
} // end conv2DAddMatrix


float* conv2DAddMatrixV2(float *data, int channels, int rows, int columns, int kernelSize){
    int padding=kernelSize-1,padding2=padding/2,newColumns=columns+padding,newRows=rows+padding,dataNewPosition=padding2*(newColumns+1),columnPosition=padding2,rowPosition=padding2;
    char flagZero, flagBegin=1;
    float *dataNew=(float*)calloc(channels*newRows*newColumns,sizeof(float)),*ptrDataNew=&dataNew[0]+padding2*(newColumns+1),*ptrData=&data[0];
    while(dataNewPosition<(channels*newRows*newColumns)-padding2*(newColumns+1)){
        *ptrDataNew=*ptrData;
        ++ptrData;
        rowPosition++;
        if(rowPosition==columns+padding2){
            rowPosition=padding2;
            columnPosition++;
            if(columnPosition==rows+padding2){
                columnPosition=padding2;
                ptrDataNew+=(padding*(newColumns+1))+1;
                dataNewPosition+=(padding*(newColumns+1))+1;
            }else{
                ptrDataNew+=padding+1;
                dataNewPosition+=padding+1;
            }
        }else{
            ++ptrDataNew;
            dataNewPosition++;
        }
    } // end for dataNewPosition: data_new
    return dataNew;
} // end conv2DAddMatrixV2