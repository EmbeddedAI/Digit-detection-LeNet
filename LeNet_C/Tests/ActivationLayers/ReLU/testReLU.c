#include <stdlib.h>
#include <stdio.h>

/**************************************************************************
*                           Protoypes Functions                           *
**************************************************************************/
void printData(long double*,int,int,int,int);
char test(long double*,long double*,int,int,int,int);
/**************************************************************************
*                        Layers Protoypes Functions                       *              
**************************************************************************/
long double *reLU(long double*,int,int,int);

int main(){
    int filters=2,rows=5,columns=5;
    //float dataInput[50] = {41.7,-11.6,-39.6,-16.2,-48.0,39.2,-19.1,-17.0,-11.9,43.9,23.8,8.8,-0.6,45.4,-17.6,-43.0,-5.4,28.8,-48.9,13.1,3.2,-38.5,3.2,-34.6,-21.8,-44.5,-22.7,-22.3,-4.7,36.2,-2.1,-24.2,-18.0,20.5,2.2,20.7,-35.4,7.6,30.8,-41.2,-18.7,20.1,-14.0,17.4,22.4,-46.1,17.4,40.0,-19.9,-43.9};
    long double dataInput[50] = {39.707030147938600000,-33.481388526197600000,18.704569209600900000,2.470226111766570000,-3.706637953119350000,17.369780118041900000,2.620288200737210000,-38.208789850899400000,15.665297787688300000,-30.963417537941000000,-25.377769652804700000,-5.619809429807340000,-41.383911630037400000,-16.850051709408100000,42.646134060973300000,29.424514094930400000,-46.599490335801900000,-39.407913755120100000,-11.765787157830700000,7.646865819936990000,49.494186627804100000,2.230667778862450000,-24.244070559788700000,0.937112490774283000,15.480473318270800000,-25.904407959017600000,-36.198449492677900000,-21.362817178235100000,27.845195700421700000,-21.848341852642800000,-0.651050236000813000,24.585312148991400000,-34.672521960140500000,-1.697901598459280000,41.396358684271800000,9.568749968232920000,19.318387793421900000,5.160628087079430000,-45.474567389689600000,-35.903903592003500000,-32.674293623887000000,-49.279469730495400000,23.425975514326700000,35.335511559935500000,-6.190423028030880000,38.597440027604200000,-40.734300760064400000,-33.458880751780000000,-39.341596666995600000,44.405359808117000000};
    long double dataTest[50] = {39.707030147938600000,0.000000000000000000,18.704569209600900000,2.470226111766570000,0.000000000000000000,17.369780118041900000,2.620288200737210000,0.000000000000000000,15.665297787688300000,0.000000000000000000,0.000000000000000000,0.000000000000000000,0.000000000000000000,0.000000000000000000,42.646134060973300000,29.424514094930400000,0.000000000000000000,0.000000000000000000,0.000000000000000000,7.646865819936990000,49.494186627804100000,2.230667778862450000,0.000000000000000000,0.937112490774283000,15.480473318270800000,0.000000000000000000,0.000000000000000000,0.000000000000000000,27.845195700421700000,0.000000000000000000,0.000000000000000000,24.585312148991400000,0.000000000000000000,0.000000000000000000,41.396358684271800000,9.568749968232920000,19.318387793421900000,5.160628087079430000,0.000000000000000000,0.000000000000000000,0.000000000000000000,0.000000000000000000,23.425975514326700000,35.335511559935500000,0.000000000000000000,38.597440027604200000,0.000000000000000000,0.000000000000000000,0.000000000000000000,44.405359808117000000};
    printf("Data\n");
    printData(dataInput,filters,1,rows,columns);
    long double *data = reLU(dataInput,filters,rows,columns);
    printf("Result: ");
    if(test(data,dataTest,filters,1,rows,columns))
        printf("Error\n");
    else
        printf("OK\n");
    printData(data,filters,1,rows,columns);
    return 0;
} // end main

/**************************************************************************
*   Function:   printData()
*   Purpose:    This is the function for test this layer.
*   Arguments:
*       data:       Array with the output data of this layer.
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       void
**************************************************************************/
void printData(long double* data, int filters, int channels, int rows, int columns){
    int filterPostion,channelPosition,columnPosition,rowPosition,dataPosition=0;
    for(filterPostion = 0;filterPostion<filters;++filterPostion){
        for(channelPosition = 0;channelPosition<channels;++channelPosition){
            for(columnPosition = 0;columnPosition<rows;++columnPosition){
                printf(" ");
                for(rowPosition = 0;rowPosition<columns;++rowPosition){
                    printf("%.18Lf ",data[dataPosition]);
                    dataPosition++;
                } // end for rowPosition: newColumns
                printf("\n");
            } // end for columnPosition: newRows
            printf("\n");
        } // end for channelPosition: channels
        printf("\n");
        printf("\n");
    } // end for filterPostion: filters
} // end printData

/**************************************************************************
*   Function:   test()
*   Purpose:    This is the function for test this layer.
*   Arguments:
*       data:       Array with the output data of this layer.
*       dataTest:   Array with the expected output data of this layer
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       Boolean for indicate if the result layer is Error or Correct.
*       1 - Error
*       0 - Correct
**************************************************************************/
char test(long double *data, long double *dataTest, int filters, int channels, int columns, int rows){
    for(int i = 0; i<filters*channels*columns*rows;++i)
        if(dataTest[i] != data[i])
            return 1;
    return 0;
} // end test

/**************************************************************************
 *                                 Layers                                 *
 **************************************************************************/
/**************************************************************************
 *                           Activation Layers                            *
 **************************************************************************/
/**************************************************************************
*   Function:   reLU()
*   Purpose:  Activation Layer ReLU
*   Arguments:
*       data:       Array with the input data of this activation layer.
*       filters:    Number of filters in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       Array with negative data converted to 0 and positive data equal 
*       to input.
**************************************************************************/
long double *reLU(long double *data, int filters, int rows, int columns){
    int i;
    long double *ptrData = &data[0];
    for(i = 0; i < filters*rows*columns; ++i){
        if(*ptrData < 0)
            *ptrData = 0;
        ++ptrData;
    } // for i:data
    return data;
} // end reLU