import numpy as np

arr = np.random.rand(1,8)

def softmax(sal):
    exp = np.exp(sal)
    scores = exp / np.sum(exp)
    return scores

#    EIGHT 
dataInputEight = np.random.rand(1,8)
print("Data Eight: ",dataInputEight,"\n");
data = softmax(dataInputEight);
print("Sum Eight: ",np.sum(data))
print("Result Eight: ",data,"\n");
#    TEN 
dataInputTen = np.random.rand(1,10)
print("Data Ten: ",dataInputTen,"\n");
data = softmax(dataInputTen);
print("Sum Ten: ",np.sum(data))
print("Result Ten: ",data,"\n");
dataInput = [0.383450707636671,0.616549292363329]
data = softmax(dataInput);
print("Sum Test: ",np.sum(data))
print("Result Test: ",data,"\n");