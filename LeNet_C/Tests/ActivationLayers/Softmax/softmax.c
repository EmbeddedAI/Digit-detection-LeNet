#include <stdlib.h>
#include <stdio.h>
#include <math.h>
/* Measure execution time */
#include <time.h>

/**************************************************************************
*                            Constans Defintion                           *
**************************************************************************/
#define NUM_CLASS_EIGHT 8
#define NUM_CLASS_TEN 10
/**************************************************************************
*                           Protoypes Functions                           *
**************************************************************************/
void printData(float*,int,int,int,int);
char test(float*,float*,int,int,int,int);
/**************************************************************************
*                        Layers Protoypes Functions                       *              
**************************************************************************/
float *softmax(float*,int);

int main(){
    int filters=2,rows=5,columns=5;
    float dataInputTen[NUM_CLASS_TEN] = {0.0205,0.0043,0.0105,0.0323,0.0345,0.0009,0.00134,0.989,0.045,0.0543};
    float dataTestTen[NUM_CLASS_TEN] = {0.08580205,0.08442326,0.08494831,0.08682051,0.08701173,0.08413671,0.08417374,0.22600179,0.08793017,0.08875173};
    /*  TEN */
    struct timespec begin, end; 
    long long nanoseconds;
    printf("Data Ten\n");
    printData(dataInputTen,1,1,1,NUM_CLASS_TEN);
    clock_gettime(CLOCK_REALTIME, &begin);
    float *data = softmax(dataInputTen,NUM_CLASS_TEN);
    clock_gettime(CLOCK_REALTIME, &end);
    printf("Result Ten: ");
    if(test(data,dataTestTen,1,1,1,NUM_CLASS_TEN))
        printf("Error\n");
    else
        printf("OK\n");
    printData(data,1,1,1,NUM_CLASS_TEN);
    nanoseconds = end.tv_nsec - begin.tv_nsec;
    printf("Time (ns): %lld\n",nanoseconds);
    return 0;
} // end main

/**************************************************************************
*   Function:   printData()
*   Purpose:    This is the function for test this layer.
*   Arguments:
*       data:       Array with the output data of this layer.
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       void
**************************************************************************/
void printData(float* data, int filters, int channels, int rows, int columns){
    int filterPostion,channelPosition,columnPosition,rowPosition,dataPosition=0;
    for(filterPostion = 0;filterPostion<filters;++filterPostion){
        for(channelPosition = 0;channelPosition<channels;++channelPosition){
            printf(" {");
            for(columnPosition = 0;columnPosition<rows;++columnPosition){
                printf(" [");
                for(rowPosition = 0;rowPosition<columns;++rowPosition){
                    printf("%.19f ",data[dataPosition]);
                    dataPosition++;
                } // end for rowPosition: newColumns
                printf("]\n");
            } // end for columnPosition: newRows
            printf("}\n");
        } // end for channelPosition: channels
        printf("\n");
        printf("\n");
    } // end for filterPostion: filters
} // end printData


/**************************************************************************
*   Function:   test()
*   Purpose:    This is the function for test this layer.
*   Arguments:
*       data:       Array with the output data of this layer.
*       dataTest:   Array with the expected output data of this layer
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       Boolean for indicate if the result layer is Error or Correct.
*       1 - Error
*       0 - Correct
**************************************************************************/
char test(float *data, float *dataTest, int filters, int channels, int columns, int rows){
    float comparator = 0.00000006;
    for(int i = 0; i<filters*channels*columns*rows;++i){
        if(fabsf(dataTest[i]-data[i])>comparator){
            printf("Error in: dataTest[%d] = %.19f  data[%d] = %.19f Rest= %.19f \n",i,dataTest[i],i,data[i],fabsf(dataTest[i]-data[i]));
            return 1;
        }
    }
    return 0;
} // end test


/**************************************************************************
 *                                 Layers                                 *
 **************************************************************************/
/**************************************************************************
 *                           Activation Layers                            *
 **************************************************************************/
/**************************************************************************
*   Function:   softmax()
*   Purpose:  Softmax Layer ReLU
*   Arguments:
*       output_fully_connected: Array with the output data of 
*                               fully connected.
*   Return:
*       Array with output data of activation layer
**************************************************************************/
float *softmax(float* input, int numClasses){
    int i;
    float sum_array = 0,*ptrInput = &input[0];
    for(i = 0;i<numClasses; ++i){
        *ptrInput = exp(*ptrInput);
        sum_array += *ptrInput;
        ++ptrInput;
    } // end if
    ptrInput=&input[0];
    for(i = 0;i<numClasses; ++i){
        *ptrInput /= sum_array;
        ++ptrInput;
    } // i: NUM_CLASS
    return input;
} // end softmax