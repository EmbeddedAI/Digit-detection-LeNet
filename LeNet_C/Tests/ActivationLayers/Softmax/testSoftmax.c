#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/**************************************************************************
*                            Constans Defintion                           *
**************************************************************************/
#define NUM_CLASS_EIGHT 8
#define NUM_CLASS_TEN 10
/**************************************************************************
*                           Protoypes Functions                           *
**************************************************************************/
void printData(long double*,int,int,int,int);
char test(long double*,long double*,int,int,int,int);
/**************************************************************************
*                        Layers Protoypes Functions                       *              
**************************************************************************/
long double *softmax(long double*,int);

int main(){
    int filters=2,rows=5,columns=5;
    long double dataInputEight[NUM_CLASS_EIGHT] = {0.8462719,0.35042015,0.22703687,0.06842849,0.48495449,0.82447974,0.40452249,0.28634137};
    long double dataTestEight[NUM_CLASS_EIGHT] = {0.18194506,0.11081399,0.09795123,0.08358477,0.12677164,0.17802298,0.11697443,0.10393589};
    long double dataInputTen[NUM_CLASS_TEN] = {0.08489565,0.00998057,0.49025369,0.61062292,0.64500075,0.38451857,0.27618421,0.14548975,0.64789757,0.21401111};
    long double dataTestTen[NUM_CLASS_TEN] = {0.07470162,0.06930983,0.11204043,0.12637189,0.13079182,0.10079862,0.09044938,0.07936805,0.13117125,0.08499711};
    /*  EIGHT */
    printf("Data Eight\n");
    printData(dataInputEight,1,1,1,NUM_CLASS_EIGHT);
    long double *data = softmax(dataInputEight,NUM_CLASS_EIGHT);
    printf("Result Eight: ");
    if(test(data,dataTestEight,1,1,1,NUM_CLASS_EIGHT))
        printf("Error\n");
    else
        printf("OK\n");
    printData(data,1,1,1,NUM_CLASS_EIGHT);
    /*  TEN */
    printf("Data Ten\n");
    printData(dataInputTen,1,1,1,NUM_CLASS_TEN);
    data = softmax(dataInputTen,NUM_CLASS_TEN);
    printf("Result Ten: ");
    if(test(data,dataTestTen,1,1,1,NUM_CLASS_TEN))
        printf("Error\n");
    else
        printf("OK\n");
    printData(data,1,1,1,NUM_CLASS_TEN);
    return 0;
} // end main

/**************************************************************************
*   Function:   printData()
*   Purpose:    This is the function for test this layer.
*   Arguments:
*       data:       Array with the output data of this layer.
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       void
**************************************************************************/
void printData(long double* data, int filters, int channels, int rows, int columns){
    int filterPostion,channelPosition,columnPosition,rowPosition,dataPosition=0;
    for(filterPostion = 0;filterPostion<filters;++filterPostion){
        for(channelPosition = 0;channelPosition<channels;++channelPosition){
            for(columnPosition = 0;columnPosition<rows;++columnPosition){
                printf(" ");
                for(rowPosition = 0;rowPosition<columns;++rowPosition){
                    printf("%.8Lf ",data[dataPosition]);
                    dataPosition++;
                } // end for rowPosition: newColumns
                printf("\n");
            } // end for columnPosition: newRows
            printf("\n");
        } // end for channelPosition: channels
        printf("\n");
        printf("\n");
    } // end for filterPostion: filters
} // end printData

/**************************************************************************
*   Function:   test()
*   Purpose:    This is the function for test this layer.
*   Arguments:
*       data:       Array with the output data of this layer.
*       dataTest:   Array with the expected output data of this layer
*       filters:    Number of filters in the data array.
*       channels:   Number of channels of each filter in the data array.
*       rows:       Number of rows of each channel in the data array.
*       columns:    Number of cols of each channel in the data array.
*   Return:
*       Boolean for indicate if the result layer is Error or Correct.
*       1 - Error
*       0 - Correct
**************************************************************************/
char test(long double *data, long double *dataTest, int filters, int channels, int columns, int rows){
    for(int i = 0; i<filters*channels*columns*rows;++i)
        if((int) (dataTest[i]*10000000) != (int) (data[i]*10000000))
            return 1;
    return 0;
} // end test

/**************************************************************************
 *                                 Layers                                 *
 **************************************************************************/
/**************************************************************************
 *                           Activation Layers                            *
 **************************************************************************/
/**************************************************************************
*   Function:   softmax()
*   Purpose:  Softmax Layer ReLU
*   Arguments:
*       output_fully_connected: Array with the output data of 
*                               fully connected.
*   Return:
*       Array with output data of activation layer
**************************************************************************/
long double *softmax(long double* input, int numClasses){
    int i;
    long double sum_array = 0,*ptrInput = &input[0];
    for(i = 0;i<numClasses; ++i){
        *ptrInput = exp(*ptrInput);
        sum_array += *ptrInput;
        ++ptrInput;
    } // end if
    ptrInput=&input[0];
    for(i = 0;i<numClasses; ++i){
        *ptrInput /= sum_array;
        ++ptrInput;
    } // i: NUM_CLASS
    return input;
} // end softmax