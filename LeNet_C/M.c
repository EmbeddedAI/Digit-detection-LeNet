#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**************************************************************************
*                            Constants Defintion                           *
**************************************************************************/
#define NUM_CLASS 10

/**************************************************************************
*                           Structure Defintion                           *
**************************************************************************/
typedef struct LayerInfo LayerInfo;
struct LayerInfo{
    char *name;
    char flagBias;
    int sizeBias;
    int sizeKernel;
};

/**************************************************************************
*                           Prototypes Functions                           *
**************************************************************************/
int chargeWeights(float***,float***,LayerInfo**);
int chargeDataTest(char*,float**,int,int,int);
void printData(float*,int,int,int,int);
char test(float*,float*,int,int,int,int);
/**************************************************************************
*                        Layers Prototypes Functions                       *              
**************************************************************************/
float* conv2DAddMatrix(float*,int,int,int,int);
float *sumChannels(float*,float*,float,int,int,int,int);
float *conv2DKernel(float*,float*,float*,int,int,int,int,int,int);
float* conv2D(float*,float*,float*,int,int,int,int,int,int,int,int);
float *maxPooling2D(float*,int,int,int,int,int);
float *flatten(float*,int,int,int);
float *dense(float*,float*,float*,int,int,int);
/**************************************************************************
*                  Activation Layers Prototypes Functions                  *
**************************************************************************/
float *reLU(float*,int,int,int);
float *softmax(float*,int);
int selectNumber(float*,int);

int main(int argc, char *argv[]){
    int filters,channels,rows,columns,kernelSize,poolSize,stride,newColumns,newRows,inputNeurons,outputNeurons;
    char padding=1,activation,*filename="data/outputDense_2.dat";//*filename="data/outputFlatten.dat";
    float *data,**weights,**bias;
    int positionLayer,i,numLayers;
    LayerInfo *layersInfo;
    numLayers=chargeWeights(&weights,&bias,&layersInfo);
    // 7 
    float dataInput[784] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.329411774873734,0.725490212440491,0.623529434204102,0.592156887054443,0.235294118523598,0.141176477074623,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.87058824300766,0.996078431606293,0.996078431606293,0.996078431606293,0.996078431606293,0.945098042488098,0.776470601558685,0.776470601558685,0.776470601558685,0.776470601558685,0.776470601558685,0.776470601558685,0.776470601558685,0.776470601558685,0.666666686534882,0.203921571373939,0,0,0,0,0,0,0,0,0,0,0,0,0.26274511218071,0.447058826684952,0.282352954149246,0.447058826684952,0.639215707778931,0.890196084976196,0.996078431606293,0.882352948188782,0.996078431606293,0.996078431606293,0.996078431606293,0.980392158031464,0.898039221763611,0.996078431606293,0.996078431606293,0.549019634723663,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.066666670143604,0.258823543787003,0.054901961237192,0.26274511218071,0.26274511218071,0.26274511218071,0.23137255012989,0.082352943718433,0.925490200519562,0.996078431606293,0.415686279535294,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.325490206480026,0.992156863212585,0.819607853889465,0.070588238537312,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.086274512112141,0.91372549533844,1,0.325490206480026,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.505882382392883,0.996078431606293,0.933333337306976,0.172549024224281,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.23137255012989,0.976470589637756,0.996078431606293,0.243137255311012,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.521568655967712,0.996078431606293,0.733333349227905,0.019607843831182,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.035294119268656,0.803921580314636,0.972549021244049,0.227450981736183,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.494117647409439,0.996078431606293,0.713725507259369,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.294117659330368,0.984313726425171,0.941176474094391,0.223529413342476,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.074509806931019,0.866666674613953,0.996078431606293,0.650980412960053,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.011764706112444,0.796078443527222,0.996078431606293,0.858823537826538,0.137254908680916,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.149019613862038,0.996078431606293,0.996078431606293,0.301960796117783,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.121568627655506,0.878431379795074,0.996078431606293,0.450980395078659,0.003921568859369,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.521568655967712,0.996078431606293,0.996078431606293,0.203921571373939,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.239215686917305,0.949019610881805,0.996078431606293,0.996078431606293,0.203921571373939,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.474509805440903,0.996078431606293,0.996078431606293,0.858823537826538,0.156862750649452,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.474509805440903,0.996078431606293,0.811764717102051,0.070588238537312,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    filters=20;channels=1;rows=28;columns=28;kernelSize=5;activation=1;stride=1;
    data = conv2D(dataInput,bias[0],weights[0],filters,channels,rows,columns,kernelSize,activation,stride,padding);
    poolSize=2;stride=2;
    data = maxPooling2D(data,filters,rows,columns,poolSize,stride);
    channels=filters;filters=50;columns=(columns-poolSize)/stride+1;rows=(rows-poolSize)/stride+1;kernelSize=5;activation=1;stride=1;
    data = conv2D(data,bias[1],weights[1],filters,channels,rows,columns,kernelSize,activation,stride,padding);
    stride=2;
    data = maxPooling2D(data,filters,rows,columns,poolSize,stride);
    columns=(columns-poolSize)/stride+1;rows=(rows-poolSize)/stride+1;
    printf("Columns: %d, Rows: %d, Filters: %d, InputNeurons: %d\n",columns,rows,filters,columns*rows*filters);
    data=flatten(data,filters,rows,columns);
    inputNeurons=columns*rows*filters;outputNeurons=500;columns=outputNeurons;rows=1;filters=1;
    data = dense(data,bias[2],weights[2],inputNeurons,outputNeurons,activation);
    inputNeurons=outputNeurons;outputNeurons=NUM_CLASS;columns=outputNeurons;activation=2;
    printf("InputNeurons = %d,  OutputNeurons = %d\n",inputNeurons,outputNeurons);
    data = dense(data,bias[3],weights[3],inputNeurons,outputNeurons,activation);
    /* TEST RESULT */
    printf("Result: ");
    //printData(data,filters,1,rows,columns);
    printData(data,1,1,1,outputNeurons);
    printf("Rows: %d  Columns: %d, Filters: %d\n",rows,columns,filters);
    float *dataTest;
    chargeDataTest(filename,&dataTest,1,1,outputNeurons);
    chargeDataTest(filename,&dataTest,filters,rows,columns);
    if(test(data,dataTest,1,1,1,outputNeurons))
        printf("Error\n");
    else
        printf("OK\n");
    printf("Number Recognized: %d\n",selectNumber(data,NUM_CLASS));
    return 0;
} // end main